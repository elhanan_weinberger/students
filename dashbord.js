function goToDashbord(data, isr_, schools) {
  // $("#mymodal4").modal("show");
  $('#spinnerModal').css('visibility','visible');
        
  $("#container_box3").css("display", "none");
  $("#box2").css("display", "none");
  $("#box4").css("display", "flex");
  $("#box3").css("width", "75%");
  $("#svg5").css("display", "block");
  $("#my_dataviz").css("display", "flex");

  gotoChartOfSchools(schools, data);
  // dataToChart(data);
  defaultChartLastWeek(isr_, schools);
}

function goToTravels() {
  $("#box2").css("display", "block");
  $("#container_box3").css("display", "flex");
  $("#my_dataviz").css("display", "none");

  $("#box3").css("width", "70%");
  $("#box4").css("display", "none");
}

function dataBetweenDates(isr, date1, date2, schools) {
  // $("#mymodal4").modal("show");
  $('#spinnerModal').css('visibility','visible');
        
  
  // console.log(schools);
  // console.log(date1, date2);
  $("#svg4").empty();
  $("#svg5").empty();

  var start = new Date(date1);
  var end = new Date(date2);

  let newminTime = Date.parse(`${date1} 06:00:00`);
  let newmaxTime = Date.parse(`${date2} 18:00:00`);

  console.log(newminTime);
  console.log(newmaxTime);

  console.log(new Date(newminTime));
  console.log(new Date(newmaxTime));

  var a = d3.timeDay.count(start, end);
  let NumberOfSaturday = 0;
  let array = [newminTime];
  for (let i = 0; i < a; i++) {
    array[i] = newminTime + i * 86400000;
    if (new Date(array[i]).getDay() == 6) {
      NumberOfSaturday += 1;
    }
  }
  console.log(array);
  let dataToChartFromDB = [];
  isr.ajax.server.get({
    code: 7303,
    query: {
      minTime: newminTime,
      maxTime: newmaxTime,
    },
    done: function (data) {
      
      // console.log(data);
      dataToChartFromDB = Object.entries(data.data).reduce(
        function (acc, x) {
          if (x[0].indexOf("history rides") === 0)
            acc.history.push({
              organization: x[0].replace("history rides", "").trim(),
              data: x[1],
            });
          else if (x[0].indexOf("history undefined pickups") === 0)
            acc.undf.push({
              organization: x[0]
                .replace("history undefined pickups", "")
                .trim(),
              data: x[1],
            });
          return acc;
        },
        { history: [], undf: [] }
      );

      // console.log(dataFromAmram);

      // console.log(data.data["history rides "]);
      // dataToChartFromDB = data.data["history rides "];
      let newDataHistory = dataToChartFromDB.history[0].data;
      // console.log(newDataHistory);

      gotoChartOfSchools(schools, newDataHistory, a - NumberOfSaturday);

      // console.log(array);
      let arrayOfDatesSelected = [];
      array.forEach((date) => {
        let newDate = new Date(date);
        let newDateUpdated = newDate.toLocaleDateString("en-US");
        arrayOfDatesSelected.push(newDateUpdated);
      });
      console.log(arrayOfDatesSelected);

      let arrayOfObjectsSelected = [];

      // arrayOfObjectsSelected[0] =  data.data["history rides"].filter(
      //   (travel) => {
      //     return (
      //       travel.start_time > newminTime &&
      //       travel.start_time < newminTime + 86400000
      //     );
      //   }
      // );

      for (let i = 0; i < a; i++) {
        let timestammpOfFirstDay = newminTime + i * 86400000;
        let nextTimestamp = newminTime + (i + 1) * 86400000;
        // flag = timestammpOfThatDay;
        let travelsInOneDayFromSelected = newDataHistory.filter((travel) => {
          return (
            travel.start_time > timestammpOfFirstDay &&
            travel.start_time < nextTimestamp
          );
        });
        console.log(travelsInOneDayFromSelected);
        let counter = 0;
        travelsInOneDayFromSelected.forEach((travel) => {
          if (travel.Passengers) {
            counter += travel.Passengers.length;
            travel.Passengers.forEach((passenger) => { if ( schools[passenger.school]){
              schools[passenger.school].validations += 1};
            });
          }
        });

        let ObjectDayFromSelected = {
          id: i,
          date: arrayOfDatesSelected[i],
          studentsOnTravels: counter,
          travels: travelsInOneDayFromSelected.length,
        };
        // flag = travelsInOneDayFromWeek.length;
        arrayOfObjectsSelected.push(ObjectDayFromSelected);
        console.log(arrayOfObjectsSelected);

        Object.values(schools).forEach((school) => {
          let percent = (school.students_on_bus / school.student.length) * 100;
          school.percent = percent;
        });

        console.log(schools);

        $("#svg4").empty();
        // set the dimensions and margins of the graph
        var svg = d3.select("#svg4"),
          margin = 200,
          width = 600,
          height = 300;
        var xScale = d3.scaleBand().range([0, width]).padding(0.4),
          yScale = d3.scaleLinear().range([height, 0]);

        var g = svg
          .append("g")
          .attr("transform", "translate(" + 100 + "," + 100 + ")");

        goto(arrayOfObjectsSelected);
        function goto(arrayOfObjectsSelected) {
          xScale.domain(
            arrayOfObjectsSelected.map(function (d) {
              return new Date(d.date).toLocaleDateString();
            })
          );
          yScale.domain([
            0,
            d3.max(arrayOfObjectsSelected, function (d) {
              return d.travels;
            }),
          ]);

          svg
            .append("g")
            .attr("class", "axis axis--y")
            .attr("transform", "translate(-15," + height + ")")
            .call(
              d3
                .axisLeft(yScale) // .tickFormat(function (d) {
                //   return d;
                // })

                .ticks(10)
            )
            .style("font-size", "15")
            .attr("transform", "translate(100,100)");

          g.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(xScale));

          g.append("g")
            .call(
              d3
                .axisLeft(yScale)
                .tickFormat(function (d) {
                  return d.studentsOnTravels;
                })
                .ticks(10)
            )
            .style("font-size", "15");

          g.selectAll(".bar")
            .data(arrayOfObjectsSelected)
            .enter()
            .append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
              return xScale(new Date(d.date).toLocaleDateString());
            })
            .attr("y", function (d) {
              return yScale(d.travels);
            })
            .attr("width", xScale.bandwidth())
            .attr("height", function (d) {
              return height - yScale(d.travels);
            })

            .on("mouseover", function (d, i) {
              tooltip
                .html(`נסיעות: ${d.travels}<br> עלו: ${d.studentsOnTravels} `)
                .style("visibility", "visible");
              // d3.select(this).attr("fill", shadeColor(bar_color, -15));
            })
            .on("mousemove", function () {
              tooltip
                .style("top", event.pageY - 10 + "px")
                .style("left", event.pageX + 10 + "px");
            })
            .on("mouseout", function () {
              tooltip.html(``).style("visibility", "hidden");
              // d3.select(this).attr("fill", bar_color);
            });

          const tooltip = d3
            .select("body")
            .append("div")
            .attr("class", "d3-tooltip")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("padding", "15px")
            .style("background", "rgba(0,0,0,0.6)")
            .style("border-radius", "5px")
            .style("color", "#fff")
            .text("a simple tooltip");

            svg
            .append("text")
            .attr("x", 350)
            .attr("y", 50)
            .attr("text-anchor", "middle")
            .style("font-size", "16px")
            .style("text-decoration", "underline")
            .text("פירוט נסיעות לפי תאריכים נבחרים");


            svg.selectAll("rect")
            .transition()
            .duration(800)
            .attr("y", function(d) { return y(d.travels); })
            .attr("height", function(d) { return height - y(d.travels); })
            .delay(function(d,i){console.log(i) ; return(i*100)})
        
        





        }
      }
    },
  });
  // $("#mymodal4").modal("hide");
  $('#spinnerModal').css('visibility','hidden');
        
}

function gotoChartOfSchools(schools, data, numberOfDays) {
  console.log(schools);
  console.log(data);
  $("#svg5").empty();
  let data3 = data;

  let newSchools = {};
  class NewSchool {
    constructor(school) {
      $.extend(true, this, school);
      this.studentsEntryToBus = [];
      this.travels = [];
    }
  }

  data3.forEach((travel) => {
    if (travel.Passengers) {
      travel.Passengers.forEach((passenger) => {
        let school =
          newSchools[passenger.school] ||
          (newSchools[passenger.school] = new NewSchool({
            name: passenger.school,
          }));
        school.studentsEntryToBus.push(passenger);
      });
    }
  });

  let array = Object.values(schools);
  console.log(newSchools);
  let arrayOfUpdateSchools = [];
  Object.values(newSchools).forEach((school) => {
    let updateschool = array.find((school1) => {
      return school1["name"] == school.name;
    });
    let newUpdateSchool = { ...school, ...updateschool };
    let average = newUpdateSchool.student.length * numberOfDays;
    newUpdateSchool.percent = (
      (newUpdateSchool.studentsEntryToBus.length / average) *
      100
    ).toFixed(2);
    arrayOfUpdateSchools.push(newUpdateSchool);
  });
  console.log(arrayOfUpdateSchools);

  let data2 = arrayOfUpdateSchools;

  // margin = 200,
  // width = 600,
  // height = 300;

  let margin = { top: 20, right: 20, bottom: 30, left: 40 };
  let svgWidth = 720,
    svgHeight = 400;
  let height = svgHeight - margin.top - margin.bottom,
    width = svgWidth - margin.left - margin.right;

  var svg = d3.select("#svg5");

  var xScale = d3.scaleBand().range([0, width]).padding(0.1),
    yScale = d3.scaleLinear().range([height, 0]);

  var g = svg
    .append("g")
    .attr("transform", "translate(" + 100 + "," + 100 + ")");

  xScale.domain(
    data2.map(function (d) {
      return d.name;
    })
  );
  yScale.domain([0, 100]);
  // d3.max(data2, function (d) {return d.percent})
  svg
    .append("g")
    .attr("transform", "translate(10,405)")
    .attr("class", "axis axis--x")
    .call(d3.axisBottom(xScale));

  svg
    .append("g")
    .attr("class", "axis axis--y")
    .call(
      d3
        .axisLeft(yScale) // .tickFormat(function (d) {
        //   return d;
        // })

        .ticks(5)
    )
    .style("font-size", "15")
    .attr("transform", "translate(10,55)");

  let bars = svg.selectAll(".bar").data(data2).enter().append("g");

  bars
    .append("rect")
    .attr("class", "bar")
    .attr("x", function (d) {
      return xScale(d.name);
    })
    .attr("y", function (d) {
      return yScale(d.percent);
    })
    .attr("width", xScale.bandwidth())
    .attr("height", function (d) {
      return height + 55 - yScale(d.percent);
    });

  bars
    .append("text")
    .text(function (d) {
      return d.percent + "%";
    })
    .attr("x", function (d) {
      return xScale(d.name) + xScale.bandwidth() / 2;
    })
    .attr("y", function (d) {
      return yScale(d.percent) - 5;
    })
    .attr("font-family", "sans-serif")
    .attr("font-size", "14px")
    .attr("text-anchor", "middle")
    .attr("fill", "black");

  svg
    .append("text")
    .attr("x", svgWidth / 2)
    .attr("y", svgHeight / 2 - 150)
    .attr("text-anchor", "middle")
    .style("font-size", "16px")
    .style("text-decoration", "underline")
    .text("אחוזי תיקוף לפי בתי ספר");

  // bars.append("text")
  // .text(function (d) {
  //   console.log(d);
  //     return d.percent;
  // })
  // .attr("x", function (d) {
  //     return xScale(d.name) + xScale.bandwidth() / 2;
  // })
  // .attr("y", function (d) {
  //     return yScale(d.percent) - 5;
  // })
  // .attr("font-family", "sans-serif")
  // .attr("font-size", "14px")
  // .attr("fill", "black")
  // .attr("text-anchor", "middle");

  // .on("mouseover", function (d, i) {
  //   tooltip2.html(` עלו: ${d.percent} % `).style("visibility", "visible");
  //   // d3.select(this).attr("fill", shadeColor(bar_color, -15));
  // })
  // .on("mousemove", function () {
  //   tooltip2
  //     .style("top", event.pageY - 10 + "px")
  //     .style("left", event.pageX + 10 + "px");
  // })
  // .on("mouseout", function () {
  //   tooltip2.html(``).style("visibility", "hidden");
  //   // d3.select(this).attr("fill", bar_color);
  // });

  //     svg.selectAll(".text")
  //     .data(data2)
  //     .enter()
  //     .append("text")
  //     .attr("class","label")
  //     .attr("x", (function(d) { return xScale(d.name); }  ))
  //     .attr("y", function(d) { return yScale(d.percent) - 5; })
  //     .attr("dy", ".1em")
  //     .text(function(d) { return d.percent; });

  //   svg.selectAll("text")
  //      .data(data2)
  //      .enter()
  //      .append("text")
  //      .text(function(d) {
  //    return d.percent;
  // })
  //   .attr("text-anchor", "middle")
  //   .attr("fill", "white")
  //   .attr("x", function(d, i) {
  //        return i * (width / data2.length);
  //   })
  //   .attr("y", function(d) {
  //        return height - (d * 4);
  //   });

  // const tooltip2 = d3
  //   .select("body")
  //   .append("div")
  //   .attr("class", "d3-tooltip")
  //   .style("position", "absolute")
  //   .style("z-index", "10")
  //   .style("visibility", "hidden")
  //   .style("padding", "15px")
  //   .style("background", "rgba(0,0,0,0.6)")
  //   .style("border-radius", "5px")
  //   .style("color", "#fff")
  //   .text("a simple tooltip");
}

function defaultChartLastWeek(isr, schools) {
  $("#svg4").empty();

  let today = new Date();
  let realDate = today.toLocaleDateString("en-US");

  let todayTimesatmp = Date.parse(`${realDate} 06:00:00`);
  console.log(todayTimesatmp);
  let array = [new Date()];
  for (let i = 1; i < 8; i++) {
    array[i] = new Date(todayTimesatmp - i * 86400000);
  }

  console.log(array);

  var domain = d3.extent(array);
  var timeScale = d3.scaleTime().domain(domain).range([0, 600]);
  let arrayOfObjectsDay = [];

  let newminTime = Date.parse(
    `${array[7].toISOString().split("T")[0]} 06:00:00`
  );
  let newmaxTime = Date.parse(
    `${array[0].toISOString().split("T")[0]} 18:00:00`
  );
  console.log(array[0].toISOString().split("T")[0]);

  isr.ajax.server.get({
    code: 7303,
    query: {
      minTime: newminTime,
      maxTime: newmaxTime,
    },
    done: function (data) {
      console.log(data);
      console.log(Object.entries(data.data));
      let data2 = Object.entries(data.data).reduce(
        function (acc, x) {
          if (x[0].indexOf("history rides") === 0)
            acc.history.push({
              organization: x[0].replace("history rides", "").trim(),
              data: x[1],
            });
          else if (x[0].indexOf("history undefined pickups") === 0)
            acc.undf.push({
              organization: x[0]
                .replace("history undefined pickups", "")
                .trim(),
              data: x[1],
            });
          return acc;
        },
        { history: [], undf: [] }
      );

      console.log(data2.history[0].data);
      let dataToChart = data2.history[0].data;
      gotoChartOfSchools(schools, dataToChart, 7);
      // data.data["history rides"].forEach((travel) => {
      //   if (travel.Passengers) {
      //     counter += travel.Passengers.length;
      //   }
      // });
      let arrayOfObjectsWeek = [];
      let flag = 0;
      for (let i = 1; i < 8; i++) {
        let timestammpOfThatDay = todayTimesatmp - i * 86400000;
        let nextTimestamp = (i - 1) * 86400000;
        flag = timestammpOfThatDay;
        let travelsInOneDayFromWeek = dataToChart.filter((travel) => {
          return (
            travel.start_time > timestammpOfThatDay &&
            travel.start_time < todayTimesatmp - nextTimestamp
          );
        });
        console.log(travelsInOneDayFromWeek);
        let counter = 0;
        travelsInOneDayFromWeek.forEach((travel) => {
          if (travel.Passengers) {
            counter += travel.Passengers.length;
          }
        });

        let ObjectDayFromWeek = {
          id: i,
          date: array[i],
          studentsOnTravels: counter,
          travels: travelsInOneDayFromWeek,
        };
        // flag = travelsInOneDayFromWeek.length;
        arrayOfObjectsWeek.push(ObjectDayFromWeek);
        console.log(arrayOfObjectsWeek);
      }

      arrayOfObjectsWeek.reverse();

      var svg = d3.select("#svg4"),
        margin = 200,
        width = svg.attr("width") - margin,
        height = svg.attr("height") - margin;

      var xScale = d3.scaleBand().range([0, width]).padding(0.4),
        yScale = d3.scaleLinear().range([height, 0]);

      var g = svg
        .append("g")
        .attr("transform", "translate(" + 100 + "," + 100 + ")");

      goto(arrayOfObjectsWeek);
      function goto(arrayOfObjectsWeek) {
        console.log(arrayOfObjectsWeek);

        xScale.domain(
          arrayOfObjectsWeek.map(function (d) {
            return new Date(d.date).toLocaleDateString();
          })
        );
        yScale.domain([
          0,
          d3.max(arrayOfObjectsWeek, function (d) {
            return d.studentsOnTravels;
          }),
        ]);

        g.append("g")
          .attr("transform", "translate(0," + height + ")")
          .call(d3.axisBottom(xScale));

        g.append("g")
          .call(
            d3
              .axisLeft(yScale)
              .tickFormat(function (d) {
                return d.studentsOnTravels;
              })
              .ticks(10)
          )
          .style("font-size", "15")
         
        g.selectAll(".bar")
          .data(arrayOfObjectsWeek)
          .enter()
          .append("rect")
          .attr("class", "bar")
          .attr("x", function (d) {
            return xScale(new Date(d.date).toLocaleDateString());
          })
          .attr("y", function (d) {
            return yScale(d.studentsOnTravels);
          })
          .attr("width", xScale.bandwidth())
          .attr("height", function (d) {
            return height - yScale(d.studentsOnTravels);
          })

          .on("mouseover", function (d, i) {
            tooltip
              .html(
                `נסיעות: ${d.travels.length} <br> עלו: ${d.studentsOnTravels} `
              )
              .style("visibility", "visible");
            // d3.select(this).attr("fill", shadeColor(bar_color, -15));
          })
          .on("mousemove", function () {
            tooltip
              .style("top", event.pageY - 10 + "px")
              .style("left", event.pageX + 10 + "px");
          })
          .on("mouseout", function () {
            tooltip.html(``).style("visibility", "hidden");
            // d3.select(this).attr("fill", bar_color);
          });

        const tooltip = d3
          .select("body")
          .append("div")
          .attr("class", "d3-tooltip")
          .style("position", "absolute")
          .style("z-index", "10")
          .style("visibility", "hidden")
          .style("padding", "15px")
          .style("background", "rgba(0,0,0,0.6)")
          .style("border-radius", "5px")
          .style("color", "#fff")
          .text("a simple tooltip");

        svg
          .append("text")
          .attr("x", 350)
          .attr("y", 50)
          .attr("text-anchor", "middle")
          .style("font-size", "16px")
          .style("text-decoration", "underline")
          .text("פירוט נסיעות של השבוע האחרון");

          
          svg.selectAll("rect")
          .transition()
          .duration(800)
          .attr("y", function(d) { return yScale(d.studentsOnTravels); })
          .attr("height", function(d) {console.log(d); return height - yScale(d.studentsOnTravels); })
          .delay(function(d,i){console.log(i) ; return(i*100)})

        //   .on("mouseover", function (d, i) {
        //     tooltip2.html(`Data: ${d}`).style("visibility", "visible");
        //     // d3.select(this).attr("fill", shadeColor(bar_color, -15));
        //   })
        //   .on("mousemove", function () {
        //     tooltip2
        //       .style("top", event.pageY - 10 + "px")
        //       .style("left", event.pageX + 10 + "px");
        //   })
        //   .on("mouseout", function () {
        //     tooltip2.html(``).style("visibility", "hidden");
        //     d3.select(this).attr("fill", "blue");
        //   });

        // const tooltip3 = d3
        //   .select("body")
        //   .append("div")
        //   .attr("class", "d3-tooltip")
        //   .style("position", "absolute")
        //   .style("z-index", "10")
        //   .style("visibility", "hidden")
        //   .style("padding", "15px")
        //   .style("background", "rgba(0,0,0,0.6)")
        //   .style("border-radius", "5px")
        //   .style("color", "#fff")
        //   .text("a simple tooltip");

        svg
          .append("g")
          // .attr("class", "axis axis--y")
          .call(
            d3.axisLeft(yScale) // .tickFormat(function (d) {
            //   return d;
            // })

            // .ticks(10)
          )
          .style("font-size", "15")
          .attr("transform", "translate(100,100)");

        // $("#mymodal4").modal("hide");
        $('#spinnerModal').css('visibility','hidden');
        
      }
    },
  });
  // }
}

function showValidations(data) {
  $("#svg4").empty();

  data.forEach((travel) => {
    if (travel.Passengers) {
      Passengers.forEach((passenger) => {
        schools[passenger.school].validations.push(passenger);
      });
    }
  });
  console.log(schools);
}

// Object.entries(temp1).reduce(function(acc, x){
//   if(x[0].indexOf('history rides') === 0)
//       acc.history.push({
//           organization: x[0].replace('history rides', '').trim(),
//           data: x[1]
//       });
//   else if (x[0].indexOf('history undefined pickups') === 0)
//       acc.undf.push({
//           organization: x[0].replace('history undefined pickups', '').trim(),
//           data: x[1]
//       });
//   return acc;
// }, {history: [], undf: []})
// {history: Array(1), undf: Array(1)}
// history: Array(1)
// 0: {organization: 'מועצה אזורית גוש עציון', data: Array(23)}
// length: 1
// [[Prototype]]: Array(0)
// undf: [{…}]
// [[Prototype]]: Object
