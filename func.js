(function (isr, window) {
  let ArrOfAllTravels = [];
  let schoolInTable = "";

  $(document).ready(function () {
    // $("#mymodal4").modal("show");
    $("#spinnerModal").css("visibility", "visible");

    let nextminTime;
    let travelsFromEfrat = [];
    let schools = {};
    let Alltravels = [];
    let AllStudents;
    let AllOfTracks = [];
    let arrayOfStudents = [];
    let dateSelected;
    let undefinedTravels = [];
    let AllTravelsWithUndefined = [];
    let dataToPutInTable = [];
    let StudentsOnAllBuses = 0;
    let AllStudentsOfProject = 0;
    let notConfirms = 0;
    let AllStudentsOnThisDay = 0;
    let isr_ = isr;
    let dateStart;
    let dateSelectedEnd;
    let choisenSchool = undefined;
    let newdateStart;
    let newdateEnd;
    let timestampStartDate;
    let timestampEndDate;
    let BusesNoRegular = 0;
    let BusesRegular = 0;
    let BusesNoStudents = 0;
    let travelsFromPlaning;
    let Allrte;
    // console.log(isr);
    // isr.ajax.server.get({
    //   code: 7305,
    //   query: {
    //     cardId: "555",
    //     name: "hatamar",
    //     trackName: "891147",
    //     minTime: 0,
    //     maxTime: 0,
    //     isFirstTime: true,
    //   },
    //   done: function (data) {
    //     console.log(data);
    //     undefinedTravels = data.data["new undefined pickups"];
    //     console.log(data.data["next min. time"]);
    //     nextminTime = data.data["next min. time"];
    //     Alltravels = data.data["new rides"].concat(undefinedTravels);
    //     travelsFromPlaning = data.data["new rides"];
    //     let travelInThatTime = travelsFromPlaning.find(
    //       (travel) => travel.startTime > Date.now()
    //     );
    //     // let numberOfTravel =  travelsFromPlaning.indexOf(travelInThatTime)

    //     // findindex

    //     console.log(Date.now());
    //     console.log(nextminTime);
    isr.ajax.server.get({
      code: 7314,
      query: {},
      done: function (data) {
        // console.log(data);
        Allrte = data;
        isr.ajax.server.get({
          code: 7301,
          query: {
            cardId: "555",
            name: "hatamar",
            trackName: "891147",
          },
          done: function (data) {
            AllStudents = data.data;
            console.log(AllStudents);
            AllStudents.forEach(function (student) {
              let s = new Student_(student);
              let school =
                schools[s.SchoolName] ||
                (schools[s.SchoolName] = new School_({
                  name: s.SchoolName,
                }));
              school.student.push(s);
            });

            let today = new Date();
            let realDate = today.toLocaleDateString("en-US");

            isr.ajax.server.get({
              code: 7304,
              query: {},
              done: function (data) {
                let schoolsNameAndNumber = data.data;
                let initDate = new Date();
                let newInitDate = initDate.toDateString();
                let initTimestampMorning = Date.parse(newInitDate);

                isr.ajax.server.get({
                  code: 7303,
                  query: {
                    minTime: initTimestampMorning,
                    maxTime: Date.now() - 30000,
                  },
                  done: function (data) {
                    // console.log(data);
                    let dataFromDB = Object.entries(data.data).reduce(
                      function (acc, x) {
                        if (x[0].indexOf("history rides") === 0)
                          acc.history.push({
                            organization: x[0]
                              .replace("history rides", "")
                              .trim(),
                            data: x[1],
                          });
                        else if (
                          x[0].indexOf("history undefined pickups") === 0
                        )
                          acc.undf.push({
                            organization: x[0]
                              .replace("history undefined pickups", "")
                              .trim(),
                            data: x[1],
                          });
                        return acc;
                      },
                      { history: [], undf: [] }
                    );

                    Alltravels = dataFromDB.history[0].data;
                    Alltravels.push(...dataFromDB.undf[0].data);
                    // Alltravels.push(...travelsFromPlaning);
                    // console.log(Alltravels);
                    notConfirms = 0;
                    AllStudentsOnThisDay = 0;

                    Alltravels.forEach((travel) => {
                      let travelWithDescripition = Allrte.data.find(
                        (travel2) => {
                          return travel2.rte == travel.rte;
                        }
                      );
                      if (travelWithDescripition != undefined) {
                        // console.log(travelWithDescripition);
                        travel.endDestination =
                          travelWithDescripition.description.split("-")[1];
                        travel.exitPoint =
                          travelWithDescripition.description.split("-")[0];
                      }

                      // console.log(travel);
                      if (!travel.start_time) {
                        travel.start_time = 0000;
                      }
                      if (!travel.end_time) {
                        travel.end_time = 0000;
                      }

                      if (travel.destination && travel.destination !== "") {
                        schools[travel.destination].travels.push(travel);
                      }
                      if (travel.Passengers) {
                        if (travel.Passengers.length > 0) {
                          if (travel.Passengers.length < 10) {
                            travel.notRegular = true;
                            BusesNoRegular += 1;
                          } else if (travel.Passengers.length > 10) {
                            travel.notRegular = false;
                            BusesRegular += 1;
                          } else {
                            BusesNoStudents += 1;
                          }
                          travel.Passengers.forEach((passenger) => {
                            if (passenger.confirmed == false) {
                              notConfirms += 1;
                            }
                            let name = passenger.school;
                            schools[name].students_on_bus += 1;
                            // schools[name].studentsInSchool.push(passenger);
                          });
                        }
                      } else {
                        BusesNoStudents += 1;
                      }
                      // console.log(
                      //   BusesNoRegular,
                      //   BusesRegular,
                      //   BusesNoStudents
                      // );
                    });

                    initData(nextminTime);
                  },
                });
              },
            });
          },
        });
      },
    });

    // isr.ajax.server.get({
    //   code: 7315,
    //   query: {
    //      rte: '0220110E'
    //   },
    //   done: function (data) {console.log(data)}})

    //   },
    // });

    class Student_ {
      constructor(student) {
        $.extend(true, this, student);
      }
    }

    class Travel_ {
      constructor(travel, count) {
        $.extend(true, this, travel);
        this.count = count;
        // this.Passengers = [];
      }
    }

   

    class School_ {
      constructor(school) {
        $.extend(true, this, school);
        this.student = [];
        this.travels = [];
        this.students_on_bus = 0;
        this.Buses = [];
        this.studentsInSchool = [];
        this.validations = 0;
      }

      addTravel(travel) {
        this.travels.push(new Travel_(travel, 1));
      }
    }

    // let date1, date2;
    function initData(nextminTime) {
      /* default values to datepicker*/
      let defaultDates = giveDefaultValue(isr, schools);
      dateStart = defaultDates[1];
      let newdateStart = dateStart.replace("T", " ");
      timestampStartDate = Date.parse(newdateStart);
      dateSelectedEnd = defaultDates[0];
      let newdateEnd = dateSelectedEnd.replace("T", " ");
      timestampEndDate = Date.parse(newdateEnd);

      $("#divOfBuses").remove();
      $("#divOfStudents").remove();

      const selectStartDate = document.querySelector("#startDate");
      dateStart = selectStartDate.value;
      selectStartDate.addEventListener("change", (event) => {
        dateStart = event.target.value;
        // console.log(dateStart);
        selectStartDate.value = dateStart;
        let newdateStart = dateStart.replace("T", " ");
        // console.log(newdateStart);
        $("input[name='dateStart']").val(dateStart);
        timestampStartDate = Date.parse(newdateStart);
      });

      const selectEndDate = document.querySelector("#endDate");
      dateSelectedEnd = selectEndDate.value;
      selectEndDate.addEventListener("change", (event) => {
        dateSelectedEnd = event.target.value;
        selectEndDate.value = dateSelectedEnd;
        $("input[name='dateEnd']").val(dateSelectedEnd);
        console.log(dateSelectedEnd);

        let newdateEnd = dateSelectedEnd.replace("T", " ");
        console.log(newdateEnd);
        timestampEndDate = Date.parse(newdateEnd);
      });

      /*  summary of the all buses  */
      let arrOfBus = [];

      let SummaryOfBuses = Alltravels.length;

      let divOfBuses = document.createElement("div");
      let AllBusesAndStudents = document.createElement("div");
      AllBusesAndStudents.setAttribute("id", "AllBusesAndStudents");

      divOfBuses.setAttribute("id", "divOfBuses");
      divOfBuses.appendChild(
        document.createTextNode(SummaryOfBuses + "   הסעות ")
      );
      //
      if ($("#all_schools")) {
        document.getElementById("all_schools").appendChild(divOfBuses);
      }

      StudentsOnAllBuses = 0;
      Alltravels.forEach((travel) => {
        if (travel.Passengers) {
          StudentsOnAllBuses += travel.Passengers.length;
        }
      });

      AllStudentsOfProject = AllStudents.length;
      let divStudentsOnProject = document.createElement("div");
      divStudentsOnProject.appendChild(
        document.createTextNode(AllStudentsOfProject + " תלמידים")
      );
      divOfBuses.appendChild(divStudentsOnProject);

      let divOfStudents = document.createElement("div");
      divOfStudents.setAttribute("id", "divOfStudents");
      divOfStudents.appendChild(
        document.createTextNode(StudentsOnAllBuses + " עלו ")
      );

      AllBusesAndStudents.appendChild(divOfBuses);
      AllBusesAndStudents.appendChild(divOfStudents);
      document.getElementById("all_schools").appendChild(AllBusesAndStudents);

      /* summary of every school  */

      let container = document.getElementById("schools");
      container.innerHTML = "";
      function compare(a, b) {
        return b.students_on_bus - a.students_on_bus;
      }

      let arrayOfSchools = Object.values(schools);
      let newArraySchools = arrayOfSchools.sort(compare);

      newArraySchools.forEach(function (school, i) {
        let div_school = document.createElement("li");
        let name = document.createElement("div");
        let summary_div = document.createElement("div");
        let numberOfSummary = document.createElement("div");
        let Buses_div = document.createElement("div");
        let studentsOnBus = document.createElement("div");
        let travelsWithExeption = document.createElement("div");
        let AllTravelsOfSchool = document.createElement("div");

        div_school.classList.add("div_school");
        div_school.setAttribute("data-school-name", school.name);
        name.classList.add("name_school");
        name.appendChild(document.createTextNode(school.name));

        div_school.appendChild(name);

        let busesOfSchool = 0 + " הסעות  ";
        Buses_div.appendChild(document.createTextNode(busesOfSchool));
        AllTravelsOfSchool.appendChild(Buses_div);
        AllTravelsOfSchool.classList.add("AllTravelsOfSchool");

        numberOfSummary.classList.add("numOfSummary_school");

        let summary = school.student.length + " תלמידים ";
        let numberOfStudentsOnBus = school.students_on_bus + " עלו  ";
        studentsOnBus.appendChild(
          document.createTextNode(numberOfStudentsOnBus)
        );
        studentsOnBus.classList.add("studentsOnBus");
        numberOfSummary.appendChild(document.createTextNode(summary));

        numberOfSummary.classList.add("numberOfSummary");
        numberOfSummary.setAttribute("id", school.name);

        let busesOnRoad = 0;
        let BusesFinish = 0;
        let BusesWithExeption = 0;

        if (BusesWithExeption > 0) {
          travelsWithExeption.appendChild(
            document.createTextNode(BusesWithExeption + " בחריגה ")
          );
          travelsWithExeption.classList.add("travelsWithExeption");
          AllTravelsOfSchool.appendChild(travelsWithExeption);
        }
        summary_div.appendChild(numberOfSummary);
        summary_div.appendChild(studentsOnBus);

        summary_div.classList.add("summary_div");

        div_school.setAttribute("id", `schoolNumber_${i}`);
        div_school.appendChild(AllTravelsOfSchool);
        div_school.appendChild(summary_div);
        container.appendChild(div_school);
        div_school.addEventListener("click", choiseSchool);
      });

      /* details for the chart */
      let studentsOnBus = Object.values(schools);
      let counter2 = 0;
      ArrOfAllTravels = [];

      // studentsOnBus = studentsOnBus.forEach((school) => {
      //   if (school.students_on_bus.length > 0) {
      //     counter2 += school.students_on_bus;
      //   }
      // });

      studentsOnBus = StudentsOnAllBuses;

      dataToPutInTable = Alltravels;

    console.log(dataToPutInTable);
      if ($.fn.DataTable.isDataTable("#example")) {
        $("#example").DataTable().destroy();
      }
      popDataTable(
        "#example",
        dataToPutInTable,
       
        isr,
        AllStudents
      );
      // $("#mymodal4").modal("hide");
      $("#spinnerModal").css("visibility", "hidden");

      getBus(dataToPutInTable);

      donuts(studentsOnBus, AllStudentsOfProject - studentsOnBus, notConfirms);

      busOfDonuts(
        BusesRegular,
        BusesNoRegular,
        BusesNoStudents,
        dataToPutInTable,
        
        isr, AllStudents
      );
      $("#showTravels").on("click", goToTravels);
      // setTimeout(updateData, 6000);

      $("#dashbord").on("click", () => {
        goToDashbord(dataToPutInTable, isr_, schools);
      });
      // $("#dashbord").on("click", ()=> { goto()});
      //goto()

      // dataToPutInTable, isr_, schools, data2
    }

    function choiseSchool(e) {
      $("#tableTravels").css("color", "black");
      console.log(dataToPutInTable);
      let all_schools = document.getElementById("all_schools");
      all_schools.style.background = "none";
      let name2 = this.getAttribute("data-school-name");
      console.log(name2);
      schoolInTable = schools[name2];
      choisenSchool = schools[name2];
      dataToPutInTable = schools[name2].travels || [];
      console.log(dataToPutInTable);

      let ul = document.getElementById("schools");
      let li = ul.getElementsByTagName("li");

      for (let i = 0; i < li.length; i++) {
        li[i].style.background = "";
      }
      this.style.background = "#CBE0E9";

      let name = this.getAttribute("data-school-name");
      let school = schools[name];
      if (!school.travels) {
        alert("אין נסיעות פעילות כרגע");
      } else {
        if ($.fn.DataTable.isDataTable("#example")) {
          $("#example").DataTable().destroy();
        }
        popDataTable(
          "#example",
          school.travels,
         
          isr,
          AllStudents
        );

        BusesRegular = 0;
        BusesNoRegular = 0;
        BusesNoStudents = 0;
        let counterOfNotConfirms = 0;
        schoolInTable.travels.forEach((travel) => {
          switch (travel) {
            case travel.notRegular == true:
              BusesNoRegular += 1;
              break;
            case travel.notRegular == false:
              BusesRegular += 1;
              break;
          }
          if (travel.Passengers) {
            travel.Passengers.forEach((passenger) => {
              if (passenger.confirmed == false) {
                counterOfNotConfirms += 1;
              }
            });
          } else BusesNoStudents += 1;
        });

        donuts(
          schoolInTable.students_on_bus,
          schoolInTable.students - schoolInTable.students_on_bus,
          counterOfNotConfirms
        );
        busOfDonuts(
          BusesRegular,
          BusesNoRegular,
          BusesNoStudents,
          dataToPutInTable,
         
          isr, AllStudents
        );
      }
    }

    console.log(schoolInTable);
    $("#details_students").on("click", getModalOfAllStudents);
    function getModalOfAllStudents() {
      let arrayOfStudents = [];
      console.log(dataToPutInTable);

      dataToPutInTable.forEach((travel) => {
        if (travel.Passengers) {
          let numberOfTravel = travel.vid.replace("מועצה מקומית אפרת/", "");

          travel.Passengers.forEach((passenger) => {
            passenger["numberOfTravel"] = numberOfTravel;
            arrayOfStudents.push(passenger);
          });
        }
      });
      let updateAllStudents = [];
      arrayOfStudents.forEach((student) => {
        let newstudent = AllStudents.find(
          (student1) => student1.TZ == student.TZ
        );

        updateAllStudents.push({ ...student, ...newstudent });
      });

      $("#myModal2").modal("show");
      $("#myModal2").modal({
        backdrop: "static",
        keyboard: false,
      });

      // $("#studentsDetails").empty();

      if ($.fn.DataTable.isDataTable("#tableOfAllStudents")) {
        $("#tableOfAllStudents").DataTable().clear().destroy();
      }
      popDataTable2("#tableOfAllStudents", updateAllStudents, AllStudents);
      // updateAllStudents.forEach((student) => {
      //   let tr = document.createElement("tr");

      //   let pickupTime = new Date(student.pickup_time);
      //   let time =
      //     pickupTime
      //       .getHours()
      //       .toString()
      //       .replace(/^(\d)$/, "0$1") +
      //     ":" +
      //     pickupTime
      //       .getMinutes()
      //       .toString()
      //       .replace(/^(\d)$/, "0$1");
      //   console.log(time);
      //   if (time == "undefined") {
      //     time = "-";
      //   }

      //   let sign =
      //     student.confirmed == true
      //       ? `<span id="signTrue">V</span>`
      //       : `<span id="signFalse">x</span>`;

      //   [
      //     student["Full name"],
      //     student.school,
      //     time,
      //     student.StationName,
      //     student.CardNumber,
      //     student.numberOfTravel,
      //   ].forEach((detail) => {
      //     let td = document.createElement("td");
      //     td.append(document.createTextNode(detail));
      //     tr.appendChild(td);
      //   });
      //   $("#studentsDetails").append(tr);
      // });

      $("#myModal2").on("shown.bs.modal", function () {
        $(".dataTables_filter").css("display", "block");
        $("#example_filter").css("display", "none");

        // $("#tableOfAllStudents").DataTable({
        //   paging: false,
        //   dom: "Blfrtip",
        //   language: {
        //     info: "מציג דף _PAGE_ מתוך _PAGES_",
        //     search: "חיפוש:",
        //   },
        //   buttons: [
        //     {
        //       extend: "excel",
        //       className: "btn btn-primary glyphicon glyphicon-list-alt",

        //       exportOptions: {
        //         columns: [":visible :not(.notShowPrint)"],
        //       },
        //     },
        //   ],
        //   retrieve: true,
        //   filter: true,
        //   searching: true,
        // });
      });
    }

    $("#all_schools").on("click", function getChart() {
      dataToPutInTable = AllOfTracks;
      AllStudentsOnThisDay = 0;
      dataToPutInTable.forEach((travel) => {
        if (travel.Passengers) {
          AllStudentsOnThisDay += travel.Passengers.length;
          travel.Passengers.forEach((passenger) => {
            if (passenger.confirmed == false) {
              notConfirms += 1;
            }
          });
        }
      });
      this.style.background = "#CBE0E9";
      let ul = document.getElementById("schools");
      let li = ul.getElementsByTagName("li");
      for (let i = 0; i < li.length; i++) {
        li[i].style.background = "";
      }

      donuts(
        AllStudentsOnThisDay,
        AllStudentsOfProject - AllStudentsOnThisDay,
        notConfirms
      );
      busOfDonuts(
        dataToPutInTable.length,
        0,
        0,
        dataToPutInTable,
   
        isr
      );
      let tbody = document.getElementById("tableTravels");
      tbody.innerHTML = "";
      $("#tableTravels").empty();
      dataToPutInTable.forEach((travel, i) => {
        let tbody = document.getElementById("tableTravels");
        tbody.appendChild(travel.createTableRow(i));
      });
      if ($.fn.DataTable.isDataTable("#example")) {
        $("#example").DataTable().destroy();
      }
      popDataTable("#example");
    });

    $("#changeDates").on("click", function historyTravels() {
      if (
        timestampStartDate > timestampEndDate ||
        dateStart == undefined ||
        dateSelectedEnd == undefined
      ) {
        alert("התאריך שהוזן אינו תקין. בחר טווח אחר.");
        return;
      } else {
        // $("#mymodal4").modal("show");
        $("#spinnerModal").css("visibility", "visible");
        $("#spinnerModal").on("shown.bs.modal", function () {
          $("#container").css("opacity", "0.5");
        });
        let newminTime = timestampStartDate;
        let newmaxTime = timestampEndDate;

        console.log(timestampEndDate);
        console.log(timestampStartDate);

        console.log(newminTime);
        console.log(newmaxTime);

        isr.ajax.server.get({
          code: 7303,
          query: {
            minTime: newminTime,
            maxTime: newmaxTime,
          },
          done: function (data) {
            // $("#mymodal4").modal("show");
            $("#spinnerModal").css("visibility", "visible");
            $("#divOfBuses").remove();
            $("#divOfStudents").remove();
            $("#divOfBuses").remove();
            $("#divOfStudents").remove();

            console.log(data.data);
            let dataFromDB = Object.entries(data.data).reduce(
              function (acc, x) {
                if (x[0].indexOf("history rides") === 0)
                  acc.history.push({
                    organization: x[0].replace("history rides", "").trim(),
                    data: x[1],
                  });
                else if (x[0].indexOf("history undefined pickups") === 0)
                  acc.undf.push({
                    organization: x[0]
                      .replace("history undefined pickups", "")
                      .trim(),
                    data: x[1],
                  });
                return acc;
              },
              { history: [], undf: [] }
            );

            // console.log(dataFromAmram);
            Object.values(schools).forEach((school) => {
              school.students_on_bus = 0;
            });
            BusesNoRegular = 0;
            BusesRegular = 0;
            BusesNoStudents = 0;
            let studentsOnBusInHisotry = 0;
            let studentsnoConfirms = 0;

            Alltravels = dataFromDB.history[0].data;
            Alltravels.forEach((travel) => {
              if (travel.Passengers) {
                studentsOnBusInHisotry += travel.Passengers.length;
                if (travel.Passengers.length > 0) {
                  travel.Passengers.forEach((passenger) => {
                    if (passenger.confirmed == false) {
                      studentsnoConfirms += 1;
                    }
                    let name = passenger.school;
                    schools[name].students_on_bus += 1;
                    // schools[name].studentsInSchool.push(passenger);
                  });
                  if (travel.Passengers.length >= 10) {
                    BusesRegular += 1;
                  } else {
                    BusesNoRegular += 1;
                  }
                }
              } else {
                BusesNoStudents += 1;
              }

              if (!travel.start_time) {
                travel.start_time = 0000;
              }
              if (!travel.end_time) {
                travel.end_time = 0000;
              }
            });

            dataToPutInTable = dataFromDB.history[0].data;
            // let newdataToPutInTable = dataToPutInTable;
            // console.log(newdataToPutInTable);
            // let arrayOfHistoryTravels = [];
            // newdataToPutInTable.forEach((travel) => {
            //   let updateTravel = new Travel_(travel);
            //   if (!updateTravel.start_time) {
            //     updateTravel.start_time = 0000;
            //   }
            //   if (!updateTravel.end_time) {
            //     updateTravel.end_time = 0000;
            //   }

            //   arrayOfHistoryTravels.push(updateTravel);
            // });
            // newdataToPutInTable = arrayOfHistoryTravels;
            // let AllStudentsOFDateSelected = 0;
            // let studentsOnBusInHisotry = 0;
            // let studentsNotEntryInHistory = [];
            // let studentsnoConfirms = 0;

            // let busesInDateSelected = 0;
            // let busesFinishedDateSelected;

            // arrayOfHistoryTravels.forEach((travel) => {
            //   if (travel.Passengers) {
            //     studentsOnBusInHisotry += travel.Passengers.length;
            //     travel.Passengers.forEach((passenger) => {
            //       if (passenger.confirmed == false) {
            //         studentsnoConfirms += 1;
            //       }
            //     });
            //   }
            // });

            // let SummaryOfBuses = arrayOfHistoryTravels.length;
            let SummaryOfBuses = dataToPutInTable.length;

            let divOfBuses = document.createElement("div");
            let AllBusesAndStudents = document.createElement("div");
            AllBusesAndStudents.setAttribute("id", "AllBusesAndStudents");

            divOfBuses.setAttribute("id", "divOfBuses");
            divOfBuses.appendChild(
              document.createTextNode(SummaryOfBuses + "   הסעות ")
            );
            document.getElementById("AllBusesAndStudents").innerHTML = "";

            // StudentsOnAllBuses = 0;
            // arrayOfHistoryTravels.forEach((travel) => {
            //   if (travel.Passengers) {
            //     StudentsOnAllBuses += travel.Passengers.length;
            //   }
            // });

            AllStudentsOfProject = AllStudents.length;
            let divStudentsOnProject = document.createElement("div");
            divStudentsOnProject.appendChild(
              document.createTextNode(AllStudentsOfProject + " תלמידים")
            );
            divOfBuses.appendChild(divStudentsOnProject);

            let divOfStudents = document.createElement("div");
            divOfStudents.setAttribute("id", "divOfStudents");
            divOfStudents.appendChild(
              document.createTextNode(studentsOnBusInHisotry + " עלו ")
            );

            AllBusesAndStudents.appendChild(divOfBuses);
            AllBusesAndStudents.appendChild(divOfStudents);
            document
              .getElementById("all_schools")
              .appendChild(AllBusesAndStudents);

            /* summary of every school  */

            let container = document.getElementById("schools");
            container.innerHTML = "";
            function compare(a, b) {
              return b.students_on_bus - a.students_on_bus;
            }
            let arrayOfSchools = Object.values(schools);
            let newArraySchools = arrayOfSchools.sort(compare);

            newArraySchools.forEach(function (school, i) {
              let div_school = document.createElement("li");
              let name = document.createElement("div");
              let summary_div = document.createElement("div");
              let numberOfSummary = document.createElement("div");
              let Buses_div = document.createElement("div");
              let studentsOnBus = document.createElement("div");
              let travelsWithExeption = document.createElement("div");
              let AllTravelsOfSchool = document.createElement("div");

              div_school.classList.add("div_school");
              div_school.setAttribute("data-school-name", school.name);
              name.classList.add("name_school");
              name.appendChild(document.createTextNode(school.name));

              div_school.appendChild(name);

              let busesOfSchool = 0 + " הסעות  ";
              Buses_div.appendChild(document.createTextNode(busesOfSchool));
              AllTravelsOfSchool.appendChild(Buses_div);
              AllTravelsOfSchool.classList.add("AllTravelsOfSchool");

              numberOfSummary.classList.add("numOfSummary_school");

              let summary = school.student.length + " תלמידים ";
              let numberOfStudentsOnBus = school.students_on_bus + " עלו  ";
              studentsOnBus.appendChild(
                document.createTextNode(numberOfStudentsOnBus)
              );
              studentsOnBus.classList.add("studentsOnBus");
              numberOfSummary.appendChild(document.createTextNode(summary));

              numberOfSummary.classList.add("numberOfSummary");
              numberOfSummary.setAttribute("id", school.name);

              // let busesOnRoad = 0;
              // let BusesFinish = 0;
              // let BusesWithExeption = 0;

              // if (BusesWithExeption > 0) {
              //   travelsWithExeption.appendChild(
              //     document.createTextNode(BusesWithExeption + " בחריגה ")
              //   );
              //   travelsWithExeption.classList.add("travelsWithExeption");
              //   AllTravelsOfSchool.appendChild(travelsWithExeption);
              // }
              summary_div.appendChild(numberOfSummary);
              summary_div.appendChild(studentsOnBus);

              summary_div.classList.add("summary_div");

              div_school.setAttribute("id", `schoolNumber_${i}`);
              div_school.appendChild(AllTravelsOfSchool);
              div_school.appendChild(summary_div);
              container.appendChild(div_school);
              div_school.addEventListener("click", choiseSchool);
            });

            donuts(
              studentsOnBusInHisotry,
              AllStudentsOfProject - studentsOnBusInHisotry,
              studentsnoConfirms
            );
            busOfDonuts(
              BusesRegular,
              BusesNoRegular,
              BusesNoStudents,
              dataToPutInTable,
             
              isr, AllStudents
            );

            putHistoryTable(dataToPutInTable);
            // $("#mymodal4").modal("hide");
            $("#spinnerModal").css("visibility", "hidden");
          },
          error: function (e) {
            console.error(e);
            alert("ישנה בעיה בשרת. נא נסה מאוחר יותר");
            // $("#mymodal4").modal("hide");
            $("#spinnerModal").css("visibility", "hidden");
          },
        });
      }
    });

    function putHistoryTable(dataToPutInTable) {
      // $("#mymodal4").modal("hide");
      $("#spinnerModal").css("visibility", "hidden");

      console.log(dataToPutInTable);

      if ($.fn.DataTable.isDataTable("#example")) {
        $("#example").DataTable().clear().destroy();
      }

      popDataTable(
        "#example",
        dataToPutInTable,
     
        isr,
        AllStudents
      );

      $("#tableTravels").css("color", "green");
    }

    function updateData() {
      let newUndefinedTravels = [];

      let newAllTravelsWithNewPickup = [];
      console.log(nextminTime);
      isr.ajax.server.get({
        code: 7305,
        query: {
          cardId: "555",
          name: "hatamar",
          trackName: "891147",
          minTime: nextminTime,
          maxTime: 0,
          isFirstTime: false,
        },
        done: function (data) {
          console.log(data);
          nextminTime = data.data["next min. time"];
          let isChange = false;
          $("#btn1").on("click", function () {
            var gValue = $(this).attr("class");
            if (gValue == "btn1") {
              $("#btn2").removeClass("btn1_all");
              $("#btn2").addClass("btn1");

              $("#btn3").removeClass("btn1_all");
              $("#btn3").addClass("btn1");

              $(this).removeClass("btn1");
              $(this).addClass("btn1_all");
            } else {
              $(this).removeClass("btn1_all");
              $(this).addClass("btn1");
            }
          });

          $("#btn2").on("click", function () {
            var gValue = $(this).attr("class");
            if (gValue == "btn1") {
              $("#btn1").removeClass("btn1_all");
              $("#btn1").addClass("btn1");
              $("#btn3").removeClass("btn1_all");
              $("#btn3").addClass("btn1");
              $(this).removeClass("btn1");
              $(this).addClass("btn1_all");
            } else {
              $(this).removeClass("btn1_all");
              $(this).addClass("btn1");
            }
          });

          $("#btn3").on("click", function () {
            var gValue = $(this).attr("class");
            if (gValue == "btn1") {
              $("#btn1").removeClass("btn1_all");
              $("#btn1").addClass("btn1");
              $("#btn2").removeClass("btn1_all");
              $("#btn2").addClass("btn1");
              $(this).removeClass("btn1");
              $(this).addClass("btn1_all");
            } else {
              $(this).removeClass("btn1_all");
              $(this).addClass("btn1");
            }
          });

          // if (data.data["new rides"].length > 0) {
          isChange = true;
          let newTravels = data.data["new rides"];
          // newTravels.forEach(
          // (travel) => {
          // Object.values(schools).forEach((school) => {
          //   if (school["school number"] == travel["school number"]) {
          //     school.addTravel(travel);
          //   } else {

          // let undefinedTravel = new Travel_(travel, 1);
          // AllOfTracks.push(undefinedTravel);
          let newAlltravels = Alltravels.concat(newTravels);
          Alltravels = newAlltravels;
          console.log(Alltravels);
          dataToPutInTable = Alltravels;
          getTravelsInMorning();
          getTravelsInEvening();

          initData(nextminTime);
        },
      });
    }
  });
  // $("#getTracksMap").on("click", getTracks);
  function getTracks() {
    // $("#realTime").css("display", "none");
    $(".dayAndInputDate").remove();
    $("#box3").empty();
    // $("#box3").removeClass("box3");
    var trackList = [];
    let markerFromServer = [];
    function onNewPos(t, a, b) {
      // console.log(t.position().lat, t.position().lng, a, b);

      trackList[t.code()] ||
        (trackList[t.code()] = {
          trackList: t,
          marker: {
            setPosition: function (p) {
              markerFromServer.push(p);
              // console.log("NEW POS", t.name(), p);
            },
          },
        }).marker.setPosition(t.position());
    }
    // console.log(trackList);
    // console.log(markerFromServer);
    parent.isr.event.on("tracknewposition", null, onNewPos);
    window["onClose"] = function () {
      parent.isr.event.off("tracknewposition", null, onNewPos);
    };

    // console.log(trackList);
    let box3a = document.createElement("div");
    let box3 = document.getElementById("box3");
    box3a.classList.add("box3map");
    // box3a.innerHTML = `<div id="map"></div>`;
    box3.appendChild(box3a);

    window["initMap"] = function () {
      let map;
      map = new google.maps.Map(document.getElementById("box3"), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 8,
      });

      // for (let i = 0; i < trackList.length; i++) {
      new google.maps.Marker({
        position: { lat: -34.397, lng: 150.644 },
        map,
        title: "Hello World!",
      });
    };

    $(".box2").empty();
    // let box2 = document.createElement("div");
    // box2.classList.add("box2");
    // box2.classList.add("schools");

    let tracks = document.createElement("div");
    tracks.classList.add("tracks");
    tracks.setAttribute("id", "tracks");
    tracks.classList.add("overflow-auto");

    // let titleSchools = document.createElement("div");
    // titleSchools.classList.add("titleSchools");
    $(".box2").html(`<div class="titleSchools">

      <p id="numberOfTracks">רכבים</p>
      
 
      <svg id="iconSearch2" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
          viewBox="0 0 16 16">
          <path fill-rule="evenodd"
              d="M2 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z" />
      </svg>
     </div>
     <div class="inputAndIcon">
      <input class="no-outline" id="inputBuses" class="inputSearch" placeholder="חיפוש">
      <i class="fa fa-search" id="iconSearch"></i>
     </div>
  
     `);

    // $("#numberOfTracks").html(`${AllOfTracks.length} רכבים `);
    // console.log(AllTravelsWithUndefined);
    let notConfirmsOnBus = 0;
    AllOfTracks.forEach((travel, i) => {
      if (travel.Passengers) {
        notConfirmsOnBus = travel.Passengers.filter(
          (passenger) => passenger.confirmed == false
        );
      }
      let div = document.createElement("li");
      div.classList.add("div_Onetrack");
      div.setAttribute("number", travel.vid);

      // div.addEventListener("click", choiseTrack);
      div.innerHTML = ` 
        
        <div class="nameAndNumber">
       <div class="numberTrack">${travel.vid} </div>
       <div class="nameTrack">${travel.name || "-"} שם בי"ס </div>
       </div>
       <div class="timeAndNumbers">
        <div class="startTime_endTime">
           <div class="startTimeTrack">${
             travel.startTime || "-"
           } שעת יציאה</div>
           <div class="endTimeTrack">${travel.endTime || "-"}שעת הגעה </div>
       </div>
       <div class="studentsOnTrack_notConfirms">
           <div class="studentsOnTrack">עלו ${
             travel.Passengers.length ? travel.Passengers.length : 0 || "0"
           }</div>
           <div class="studentsNoConfirms">דחויים ${
             notConfirmsOnBus.length || "0"
           }</div>
       </div> `;
      tracks.appendChild(div);
    });

    // $(".box2").append(titleSchools);
    $(".box2").append(tracks);
    let tracksPositions = {};
    let arrayOfTracks = [];
    parent.isr.track
      .list()
      .filter((x) => x.authorized())
      .forEach((x) => {
        tracksPositions[x.name] = x;
        // console.log(x.name(), x.position());
        arrayOfTracks.push(x);
      });
    console.log(tracksPositions);
  }
})(parent.isr, window);

window["initMapOuter"] = function () {
  if (!window["initMap"]) return setTimeout(window["initMapOuter"], 20);
  window["initMap"]();
};
