var table2;
var datatable;

function popDataTable(table, data,  isr, AllStudents) {


  // console.log(AllStudents);
  console.log(data);
  $(table).css("width", "100%");
  var L = 10;
  var divTable2 = $(table).parent("div");
  $("#example_filter").css("display", "none");
  datatable = $(table).DataTable({
    bDestroy: true,
    data: data,
    createdRow: function (row, data, dataIndex) {
      if (data.comment && data.comment == "Without SPM") {
        $(row).addClass("no_spm");
      }
    },
    columns: [
      {
        data: null,
        title: "תאריך",
        render: function (data, type, row, meta) {
          return new Date(row.start_time)
            .toLocaleString("en-GB")
            .substring(0, 10);
        },
      },

      {
        data: null,
        title: "קו",
        render: function (data, type, row, meta) {
          return row.rte ? row.rte : `--`;
        },
      },
      {
        data: null,
        title: "אוטובוס",
        render: function (data, type, row, meta) {
          return row.vid.replace("מועצה מקומית אפרת/", "");
        },
      },
      {
        data: null,
        title: "נהג",
        render: function (data, type, row, meta) {
          return row.driver_name ? row.driver_name : `-`;
        },
      },
      {
        data: null,
        title: "מוצא",
        render: function (data, type, row, meta) {
          return row.exitPoint ? row.exitPoint : `-`;
        },
      },
      {
        data: null,
        title: "יעד",
        render: function (data, type, row, meta) {
          return row.endDestination ? row.endDestination : `-`;
        },
      },

      {
        data: "start_time",
        title: "שעת יציאה",
        render: function (data, type, row, meta) {
          if (row.start_time) {
            let entryTime = new Date(row.start_time) || 0;

            let time =
              entryTime
                .getHours()
                .toString()
                .replace(/^(\d)$/, "0$1") +
              ":" +
              entryTime
                .getMinutes()
                .toString()
                .replace(/^(\d)$/, "0$1");
            if (entryTime == "Invalid Date") {
              time = "-";
            }
            return row.start_time ? time : `-`;
          } else return "-";
        },
      },
      {
        data: "end_time",
        title: "שעת הגעה",
        render: function (data, type, row, meta) {
          let TimeInDestination = new Date(row.predicted_arrival_time);
          let newTimeInDestination =
            TimeInDestination.getHours()
              .toString()
              .replace(/^(\d)$/, "0$1") +
            ":" +
            TimeInDestination.getMinutes()
              .toString()
              .replace(/^(\d)$/, "0$1");
          // if (typeof newTimeInDestination == "NaN:NaN") {
          //   newTimeInDestination = "-";
          // }

          if (newTimeInDestination == "Invalid Date") {
            newTimeInDestination = "-";
          }

          return row.end_time ? newTimeInDestination : `-`;
        },
      },
      {
        data: null,
        title: "עלו",
        render: function (data, type, row, meta) {
          return row.Passengers ? row.Passengers.length : `-`;
        },
      },
      {
        data: null,
        title: "דחויים",
        render: function (data, type, row, meta) {
          let counter = 0;
          if (row.Passengers) {
            row.Passengers.forEach((element) => {
              if (element.confirmed == false) {
                counter += 1;
              }
            });
            return counter;
          } else return 0;
        },
      },

      {
        data: null,
        title: "תלמידים",
        render: function (data, type, row, meta) {
          let image =
            '<img src="../lib/Images/students_images/iconStudents.svg"/>';
          let tdimage = document.createElement("div");
          tdimage.append(image);
          tdimage.setAttribute("id", `studentsList_${row.id}`);
          // let Image2 = image;
          // studentsImage2.classList.add('grey')
          // let tdMap = document.createElement("td");
          // tdMap.appendChild(studentsImage);
          return image;
        },
      },
      {
        data: null,
        title: "מפה",
        render: function (data, type, row, meta) {
          let image = `<img src="../lib/Images/students_images/iconMap.svg" />`;
          let tdimageMap = document.createElement("div");
          tdimageMap.append(image);
          tdimageMap.setAttribute("id", `map_${row.rte}`);
          // let Image2 = image;
          // studentsImage2.classList.add('grey')
          // let tdMap = document.createElement("td");
          // tdMap.appendChild(studentsImage);
          return image;
        },
      },
    ],

    initComplete: function () {
      $(".buttons-copy").html('<i class="fa fa-copy" />');
      $(".buttons-csv").html('<i class="fa fa-file-text-o" />');
      $(".buttons-excel").html('<i class="fas fa-download"></i>');
      $(".buttons-pdf").html('<i class="fa fa-file-pdf-o" />');
      $(".buttons-print").html('<i class="fa fa-print" />');

      $(".buttons-excel").css("color", "#0682BC");
      $(".buttons-excel").css("border", "2px solid #0682BC");
    },
    retrieve: true,
    bLengthChange: false,
    paging: false,
    autoWidth: true,
    searching: true,

    dom: "Blfrtip",
    buttons: [
      {
        extend: "excel",
        className: "Exportexcel btn btn-primary glyphicon glyphicon-list-alt",

        exportOptions: {
          columns: [":visible :not(.notShowPrint)"],
        },
      },
    ],

    // responsive: true,
    fixedHeader: true,
    sScrollX: "100%",
    ScrollCollapse: true,
    //              deferRender:    true,
    scrollCollapse: true,
    //  scroller:       true,
    scrollY: "40vh",
    // scrollX: "50vh",

    language: {
      info: "מציג דף _PAGE_ מתוך _PAGES_",
    },
    columnDefs: [
      {
        targets: 10,
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).attr("id", "cell-icon");
        },
      },
      {
        targets: 11,
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).attr("id", "cell-map");
        },
      },
      { className: "dt-right", targets: ["_all"] },
      { searchable: false, orderable: false, targets: "no-filter" },
      { orderable: false, targets: "no-order" },
      { responsivePriority: 1, targets: 0 },
      { responsivePriority: 2, targets: -1 },
      {
        targets: "_all",
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).attr("id", "cell-" + rowData.vid);
        },
      },
      // {
      //   "width" : "5%",  targets: [8,9,10,11],
      // },
      // { targets: [8,9,10,11],
      //   createdCell: function (td, cellData, rowData, row, col) {
      //     $(col).css("width", "5%");}
      // }
      // {targets: 7, data: "img", render:  function ( url, type, full) {
      //   return '<img src="../lib/Images/students_images/iconStudents.svg"/>'}}
    ],
    aaSorting: [],
    pageLength: 50,
    orderClasses: false,
    deferRender: true, //,
    ////,
    //"oLanguage": { "sSearch": '<a class="btn searchBtn" id="searchBtn"><i class="fa fa-search"></i></a>' }
  });
  table2 = datatable.buttons().container().appendTo("#buttons_box3");
  // console.log(getStudentsList);

  $("#example").on("click", "td#cell-icon", function () {
  
    console.log(AllStudents);
    let row = $(this).closest("tr");
    console.log(row);
    var data = datatable.row(row).data();
    console.log(data);
    var currentRowData = data;
    // var currentRowData = datatable.row(this).data();

    let travel = currentRowData;
    if (travel.has_spm == false) {
      alert("אין נתונים על תלמידים שעלו לנסיעה זו");
      return;
    }
    console.log(travel);
    $("#myModal").modal("show");
    $("#myModal").modal({
      backdrop: "static",
      keyboard: false,
    });
    $("#modalTable").empty();
    let countOfNoConfirms = [];
    if (travel.Passengers) {
      countOfNoConfirms = travel.Passengers.filter(
        (passenger) => passenger.confirmed == false
      );

      $("#modalNoConfirms").html(` דחויים  ${countOfNoConfirms.length} `);
    }
    let sum = travel.Passengers ? travel.Passengers.length : 0;
    $("#totalModal").html(`סה"כ עלו ${sum} תלמידים`);

    $("#numberOfTrack").html(`${travel.vid}`);

    let today = new Date();
    let realDate = today.toLocaleDateString("en-US");
    $("#realDate").html(realDate);

    if (travel.Passengers) {
      travel.Passengers.forEach((passenger) => {
        let newstudent = AllStudents.find(
          (student1) => student1.TZ == passenger.TZ
        );
        // newstudent = Object.assign(passenger, student1);
        passenger.grade = newstudent.grade;
        passenger.CardNumber = newstudent.CardNumber;
        let tr = document.createElement("tr");
        let pickupTime = new Date(passenger.pickup_time);
        console.log(pickupTime);
        let time =
          pickupTime
            .getHours()
            .toString()
            .replace(/^(\d)$/, "0$1") +
          ":" +
          pickupTime
            .getMinutes()
            .toString()
            .replace(/^(\d)$/, "0$1");
        console.log(time);
        if (time == "undefined") {
          time = "-";
        }
        let grade = passenger.grade;
        let sign =
          passenger.confirmed == true
            ? `<span id="signTrue">V</span>`
            : `<span id="signFalse">x</span>`;

        [
          passenger["Full name"],
          passenger.CardNumber || "-",
          passenger.school,
          grade,
          time,
        ].forEach((detail) => {
          let td = document.createElement("td");
          td.append(document.createTextNode(detail));
          tr.appendChild(td);
        });
        let td = document.createElement("td");
        td.innerHTML = sign;
        tr.appendChild(td);

        $("#modalTable").append(tr);
      });
    }

    // alert(currentRowData[0]) // wil give you the value of this clicked row and first index (td)
    //your stuff goes here
  });

  $('#example').on("click", "td#cell-map", function () {

    console.log(isr)
    let row = $(this).closest("tr");
    var data = datatable.row(row).data();
    console.log(data);
    let rte = data.rte.trim();
    let trackName = data.vid.split('/')[1]

    console.log(rte);
    $("#myModalMap").modal("show");

    $('#map').isrPosHistory({
      startTime: data.start_time,
      endTime: data.end_time,
      trackList: trackName
    }, (obj) => {

      // new obj.module.mapService.google.maps.Marker({
  //     //   position: { lat: 31.7348469, lng: 35.1791665 },
  //     //   title: "Hello World!",
  //     //   map: obj.module.mapService.map,
  //     //   // icon:
  //     // });

  //     // new obj.module.mapService.google.maps.Polyline({
  //     //   path: [
  //     //     { lat: 31.7348469, lng: 35.1791665 },
  //     //     { lat: 31.754868, lng: 35.156785 }
  //     //   ],
  //     //   strokeColor: "#FF0000",
  //     //   strokeOpacity: 1.0,
  //     //   strokeWeight: 2,
  //     //   map: obj.module.mapService.map
  //     // });
    });

  //   // debugger
  //   // isr.ajax.promise.server.get({
  //   //   code: 621,
  //   //   query: {
  //   //     catalogName: rte

  //   //     // "0070110E"

  //   //   }
  //   // }).then((x) => {
  //   //   if (x.data = {}) {
  //   //     alert('   אין נתונים על המסלול הנבחר. בחר רכב אחר. ')
  //   //   } else {
  //   //     console.log(x);
  //   //     let polyline = new isr.geography.Polyline(x.data.points);
  //   //     console.log(polyline)

  //   //   }

  //   // })
  })

  // scrollY: "300px",
  // scrollX: true,
  // scrollCollapse: true,
  // paging: false,
  // columnDefs: [
  //     { width: '32%', targets: [0,1] }
  // ],

  function showRoute(polyline) {
    function initMap(lat, lng) {
      let map;
      let icon = `<i class="fas fa-bus"></i>`;

      let mapProp = {
        zoom: 12,
        minZoom: 10,
        maxZoom: 15,
      };

      console.log(lat, lng);
      map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: lat, lng: lng },
        zoom: 18,
      });

      const image = "./bus.png";
      const marker = new google.maps.Marker({
        position: { lat: lat, lng: lng },
        map,
        icon: icon,
      });

      var infoContent =
        "<div class='info-window'><h2>חיפה</h2><p>קו רוחב: " +
        marker.position().lat() +
        "</p><p>קו אורך: " +
        marker.position.lng() +
        "</p></div>";

      let infoWindow = new google.maps.infoWindow({
        content: infoContent,
      });

      google.maps.event.addListener(marker, "click", function () {
        infowindow.open(map, marker);

        map.setZoom(mapProp.maxZoom);
      });

      google.maps.event.addListener(infowindow, "closeclick", function () {
        map.setZoom(mapProp.zoom);
      });
    }
  }

  // console.log(table2);
  // table2.columns.adjust().draw();
  // $(document).on("click past select keyup","#searchStudent",function(){
  //   table2.search($(this).val()).draw() ;
  // })

  //  $(tr).on('click', getStudentsList(rowData))

  //   $('#example tbody').on('click','tr', function() {
  //     var currentRowData = datatable.row(this).data();
  //     console.log(currentRowData);
  //     getStudentsList(currentRowData)
  //     // alert(currentRowData[0]) // wil give you the value of this clicked row and first index (td)
  //     //your stuff goes here
  // });

  //  $('#example tbody').on('click', 'td', function () {
  //   debugger
  //    console.log(this.rowData)
  //   let id = this.getAttribute("id");
  //   let newid = id.replace('cell-','');
  //   console.log(newid);
  //   console.log(id);
  //   let travel = dataToPutInTable.find((travel)=>{ travel.vid === newid})
  //   console.log(travel);
  //   // getStudentsList(this.rowData)
  //  })
  $("#spinnerModal").css("visibility", "hidden");
  function redraw() {
    datatable.columns.adjust().draw();
  }

  window.addEventListener("resize", redraw);

  $(table).addClass("table2");
  return datatable;
  //    table2.fnStandingRedraw();
}



function modalTableWithDataTable(table) {
  $(table).DataTable();
}

// oTable = $('#myTable').DataTable();
$("#searchStudent").keyup(function () {
  datatable.search($(this).val()).draw();
});

var table2;
var datatable2;
function popDataTable2(table, data, AllStudents) {
  // console.log(data);
  // debugger
  // let updateAllStudents = [];

  // let newstudent = AllStudents.find(
  //   (student1) => student1.TZ == student.TZ
  // );

  // updateAllStudents.push({ ...student, ...newstudent });

  // console.log(updateAllStudents)

  $(table).css("width", "100%");
  var L = 10;
  var divTable2 = $(table).parent("div");
  datatable2 = $(table).DataTable({
    bDestroy: true,
    data: data,
    createdRow: function (row, data, dataIndex) {},
    columns: [
      { data: "Full name", title: "שם" },
      {
        data: null,
        title: "בית ספר",
        render: function (data, type, row, meta) {
          return row.school ? row.school : `-`;
        },
      },
      {
        data: null,
        title: "כיתה",
        render: function (data, type, row, meta) {
          return row.grade ? row.grade : `-`;
        },
      },
      {
        data: "pickup_time",
        title: "שעת עליה",
        render: function (data, type, row, meta) {
          if (row.pickup_time) {
            let entryTime = new Date(row.pickup_time) || 0;

            let time =
              entryTime
                .getHours()
                .toString()
                .replace(/^(\d)$/, "0$1") +
              ":" +
              entryTime
                .getMinutes()
                .toString()
                .replace(/^(\d)$/, "0$1");
            if (entryTime == "Invalid Date") {
              time = "-";
            }
            return row.pickup_time ? time : `-`;
          } else return "-";
        },
      },
      {
        data: "stationMakat",
        title: "מספר תחנה",
        render: function (data, type, row, meta) {
          return row.stationMakat ? row.stationMakat : `-`;
        },
      },
      {
        data: "numberOfTravel",
        title: "מספר רכב",
        // render: function (data, type, row, meta) {
        //   return row.Passengers ? row.Passengers.length : `-`;
        // },
      },
      {
        data: null,
        title: "אישור",
        render: function (data, type, row, meta) {
          let sign =
            row.confirmed == true
              ? `<span id="signTrue">V</span>`
              : `<span id="signFalse">x</span>`;
          return sign;
        },
      },
    ],

    initComplete: function () {
      $(".buttons-copy").html('<i class="fa fa-copy" />');
      $(".buttons-csv").html('<i class="fa fa-file-text-o" />');
      $(".buttons-excel").html('<i class="fas fa-download"></i>');
      $(".buttons-pdf").html('<i class="fa fa-file-pdf-o" />');
      $(".buttons-print").html('<i class="fa fa-print" />');

      $(".buttons-excel").css("color", "#0682BC");
      $(".buttons-excel").css("border", "2px solid #0682BC");
    },
    retrieve: true,
    bLengthChange: false,
    paging: false,
    autoWidth: true,
    searching: true,

    dom: "Blfrtip",
    buttons: [
      {
        extend: "excel",
        className: "Exportexcel btn btn-primary glyphicon glyphicon-list-alt",

        exportOptions: {
          columns: [":visible :not(.notShowPrint)"],
        },
      },
    ],

    responsive: true,
    fixedHeader: true,
    //                "sScrollX": "100%",
    bScrollCollapse: true,
    //              deferRender:    true,
    scrollCollapse: true,
    //  scroller:       true,
    // scrollY: "100%",
    // scrollX: "100vh",

    language: {
      info: "מציג דף _PAGE_ מתוך _PAGES_",
    },
    columnDefs: [
      { className: "dt-right", targets: "_all" },
      { searchable: false, orderable: false, targets: "no-filter" },
      { orderable: false, targets: "no-order" },
      { responsivePriority: 1, targets: 0 },
      { responsivePriority: 2, targets: -1 },
      {
        targets: "_all",
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).attr("id", "cell-" + rowData.vid);
        },
      },
      // {targets: 7, data: "img", render:  function ( url, type, full) {
      //   return '<img src="../lib/Images/students_images/iconStudents.svg"/>'}}
    ],
    aaSorting: [],
    pageLength: 50,
    orderClasses: false,
    deferRender: true, //,
    ////,
    //"oLanguage": { "sSearch": '<a class="btn searchBtn" id="searchBtn"><i class="fa fa-search"></i></a>' }
  });

  function redraw() {
    datatable.columns.adjust().draw();
  }

  window.addEventListener("resize", redraw);

  $(table).addClass("table2");
  return datatable2;
  //    table2.fnStandingRedraw();
}

// function modalTableWithDataTable(table) {
//   $(table).DataTable();
// }

// // oTable = $('#myTable').DataTable();
// $("#searchStudent").keyup(function () {
//   datatable.search($(this).val()).draw();
// });
