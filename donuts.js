const donuts = function (studentsOnBus, studentsDontEntry, NoConfirms,   dataToPutInTable,
  
  isr) {
  $("#svg").remove();
  // $("#svg2").remove();
  // $("#divOfBuses").remove();
  // $("#divOfStudents").remove();
  // let StudentsNotOnBus = studentsWriteToBus - studentsOnBus;

  var data = [
    {
      name: "לא עלו",
      selector: "warning",
      count: studentsDontEntry,
      color: "#b8b8b8",
      type: 'noEntry'
    },
    {
      name: "עלו",
      selector: "success",
      count: studentsOnBus,
      color: "#0681bb",
      type: 'entry'
    },
    { name: "דחויים", selector: "error", count: NoConfirms, color: "#c94646", type: 'noConfirms' },
    { name: "פירוט לפי נוסע", selector: "primary", count: 0, color: "#0681bb", type:'details' },
  ];

  data.map(
    (o) =>
      (document.getElementById(`counter-${o.selector}-students`).innerHTML =
        o.count)
  );

  var total = data.reduce(function (acc, obj) {
    return acc + obj.count;
  }, 0);

  var width = 300,
    height = 200,
    radius = 90;

  var arc = d3
    .arc()
    .outerRadius(radius - 1)
    .innerRadius(60);

  var pie = d3
    .pie()
    .sort(null)
    .value(function (d) {
      return d.count;
    });

  var svg = d3
    .select("#chart")
    .append("svg")
    .attr("id", "svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  var g = svg.selectAll(".arc").data(pie(data)).enter()

  g.append("path")
    .attr("d", arc)
    .style("fill", function (d, i) {
      return d.data.color;
    });
  // g.append("text").attr("text-anchor", "right").text("תלמידים")
  g.append("text")
    .attr("text-anchor", "middle")
    .text(`עלו סה"כ`)
    .attr("style", "transform: translateY(-27px)");
  g.append("text")
    .attr("dy", "1em")
    .attr("text-anchor", "middle")
    .attr("style", "font-size: 2em")
    // .text(total)
    .text(studentsOnBus)
    .attr("style", "font-size: 2em; transform: translateY(-27px)");
  g.append("text")
    .attr("dy", "3em")
    .attr("text-anchor", "middle")
    .text("תלמידים")
    .attr("style", "transform: translateY(-27px)");







  // var button = g.append("");
};
