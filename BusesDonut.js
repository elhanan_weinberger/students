const busOfDonuts = function (
  busesRegular,
  BusesNoRegular,
  BusesNoStudents,
  dataToPutInTable,
  
  isr,
  AllStudents
) {
  console.log(AllStudents);
  $("#svg2").remove();

  var data = [
    {
      name: "מספר נוסעים לא תקין",
      selector: "warning",
      count: BusesNoRegular,
      color: "#b8b8b8",
      type: "noRegular",
    },
    {
      name: "מספר נוסעים תקין",
      selector: "success",
      count: busesRegular,
      color: "#0681bb",
      type: "Regular",
    },
    {
      name: "לא עלו ",
      selector: "error",
      count: BusesNoStudents,
      color: "#c94646",
      type: "noStudents",
    },
  ];

  data.map(
    (o) =>
      (document.getElementById(`counter-${o.selector}`).innerHTML = o.count)
  );

  var total = data.reduce(function (acc, obj) {
    return acc + obj.count;
  }, 0);

  var width = 300,
    height = 200,
    radius = 90;

  var arc = d3
    .arc()
    .outerRadius(radius - 1)
    .innerRadius(60);

  var pie = d3
    .pie()
    .sort(null)
    .value(function (d) {
      return d.count;
    });

  var svg2 = d3
    .select("#chart2")
    .append("svg")
    .attr("id", "svg2")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

  var g = svg2
    .selectAll(".arc")
    .data(pie(data))
    .enter()
    .append("g")
    .attr("class", "arc")
    .on("click", (data) => {
     
      console.log(AllStudents);
      let type = data.data.type;
      var current = this;
      var others = svg2.selectAll(".arc").filter(function (el) {
        return this != current;
      });
      others.selectAll("path").style("opacity", 0.3);
      console.log(this);

      let arrayOfRegulars = [];
      let arrayOfNoRegulars = [];
      let arrayofNoStudents = [];
      let newData = [];

      dataToPutInTable.forEach((travel) => {
        if (travel.notRegular == false) {
          arrayOfRegulars.push(travel);
        } else {
          if (travel.notRegular == true) {
            arrayOfNoRegulars.push(travel);
          } else {
            arrayofNoStudents.push(travel);
          }
        }
      });

      $("#titlePieStudents").on("click", () => {
        console.log(this);
        
        console.log(AllStudents);
        $("#spinnerModal").css("visibility", "visible");

        // var current = this;
        // d3.select(this).style("opacity", 1);
        // var others = svg2.selectAll(".arc").filter(function (el) {
        //   return this != current;
        // });
        // others.
        d3.selectAll("path").style("opacity", 1);

        if ($.fn.DataTable.isDataTable("#example")) {
          $("#example").DataTable().destroy();
        }
        popDataTable(
          "#example",
          dataToPutInTable,
         
          isr,
          AllStudents
        );
      });

      type == "Regular"
        ? (newData = arrayOfRegulars)
        : type == "noRegular"
        ? (newData = arrayOfNoRegulars)
        : (newData = arrayofNoStudents);

      if ($.fn.DataTable.isDataTable("#example")) {
        $("#example").DataTable().destroy();
      }
      console.log(newData);
      popDataTable("#example", newData, isr, AllStudents);
    });
  // .on("click", function () {
  //   var current = this;
  //   d3.select(this).style("opacity", 1);
  //   var others = svg2.selectAll(".arc").filter(function (el) {
  //     return this != current;
  //   });
  //   others.selectAll("path").style("opacity", 1);
  //   if ($.fn.DataTable.isDataTable("#example")) {
  //     $("#example").DataTable().destroy();
  //   }
  //   popDataTable("#example", dataToPutInTable, getStudentsList, isr);
  // });

  g.append("path")
    .attr("d", arc)
    .style("fill", function (d, i) {
      return d.data.color;
    });
  g.append("text")
    .attr("text-anchor", "middle")
    .text(`סה"כ`)
    .attr("class", "all")
    .attr("style", "transform: translateY(-27px)");
  g.append("text")
    .attr("dy", "1em")
    .attr("text-anchor", "middle")
    .attr("style", "font-size: 2em")
    .text(total)
    .attr("style", "font-size: 2em; transform: translateY(-27px)");
  g.append("text")
    .attr("style", "transform: translateY(-27px)")
    .attr("dy", "3em")
    .text("הסעות")
    .attr("text-anchor", "middle");
};
