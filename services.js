function giveDefaultValue(isr, schools) {
 
  console.log(isr); 
  console.log(schools);
  var now = new Date();
  var utcString = now.toISOString().substring(0, 19);
  var year = now.getFullYear();
  var month = now.getMonth() + 1;
  var day = now.getDate();
  var hour = now.getHours();
  var minute = now.getMinutes();
  var second = now.getSeconds();

  var localDatetime =
    year +
    "-" +
    (month < 10 ? "0" + month.toString() : month) +
    "-" +
    (day < 10 ? "0" + day.toString() : day) +
    "T" +
    (hour < 10 ? "0" + hour.toString() : hour) +
    ":" +
    (minute < 10 ? "0" + minute.toString() : minute);
  // +      utcString.substring(16, 19);
  console.log(localDatetime);
  console.log(utcString.substring(16, 19));

  var localDatetime2 =
    year +
    "-" +
    (month < 10 ? "0" + month.toString() : month) +
    "-" +
    (day < 10 ? "0" + day.toString() : day) +
    "T" +
    "06:00";
  //  + utcString.substring(16, 19);
  // (hour < 10 ? "0" + hour.toString() : hour) +
  // ":" +
  // (minute < 10 ? "0" + minute.toString() : minute) +
  // utcString.substring(16, 19);

  var datetimeField2 = document.getElementById("startDate");
  var datetimeField = document.getElementById("endDate");
  datetimeField.value = localDatetime;
  datetimeField2.value = localDatetime2;

  let date1, date2;

  $("#dateToShowInGraph").datepicker({
    // dateFormat: "dd-mm-yy",
    // format: "dd/mm/yyyy",
    autoclose: true,
    onSelect: function (dateText, inst) {
      date1 = dateText;
      $("input[name='dateToShowInGraph']").val(dateText);
      // historyToShowInGraph(dateText);
    },
  });

  $("#dateToShowInGraph2").datepicker({
    // format: "dd/mm/yyyy",
    // dateFormat: "dd-mm-yy",
    autoclose: true,
    onSelect: function (dateText, inst) {
      date2 = dateText;
      $("input[name='dateToShowInGraph2']").val(dateText);
      // historyToShowInGraph(dateText);
      console.log(dateText);
      console.log(date2);
      console.log(isr);
      gotochart(date1, date2, schools, isr);
    },
  });

  function gotochart(date1, date2, schools, isr) {
    console.log(isr)
    console.log(schools);
    dataBetweenDates(isr, date1, date2, schools);
  }
  return [localDatetime, localDatetime2]
};


function hoverOnDonuts(){
  var dataset = {
    apples: [53245, 28479, 19697, 24037, 40245],
  };
  
  var width = 460,
      height = 300,
      radius = Math.min(width, height) / 2;
  
  var color = d3.scale.category20();
  
  var pie = d3.layout.pie()
      .sort(null);
  
               
                    
               
  var arc = d3.svg.arc()
      .innerRadius(radius - 100)
      .outerRadius(function(){
      return (d3.select(this).classed('clicked'))? (radius - 50) * 1.08
                                                                             : (radius - 50);
      });
  
  var svg = d3.select("body").append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
      
        var g = svg.selectAll(".arc")
      .data(pie(dataset.apples))
      .enter().append("g")
      .attr("class", "arc")
      .on('mouseover', function() {
        var current = this;  
        var others = svg.selectAll(".arc").filter(function(el) {
          return this != current
        });
        others.selectAll("path").style('opacity', 0.3);
      })
      .on('mouseout', function() {
        var current = this;
        d3.select(this)
          .style('opacity', 1);
        var others = svg.selectAll(".arc").filter(function(el) {
          return this != current
        });
        others.selectAll("path").style('opacity', 1);
      })
  
    g.append("path")
      .attr("d", arc)
      .style("fill", function(d, i) {
        return color(i);
      });
  
    g.append("text")
      .attr("transform", function(d) {
        return "translate(" + arc.centroid(d) + ")";
      })
      .attr("dy", ".35em")
      .style("text-anchor", "middle")
      .text(function(d) {
        console.log("d is", d);
        return percentageFormat(d.data.percentage);
      });












}















// function datepickerNew(){
//   var today = new Date();
//   var minDate = today.setDate(today.getDate() + 1);

//   $('#datePicker').datetimepicker({
//     useCurrent: false,
//     format: "MM/DD/YYYY",
//     minDate: minDate
//   });

//   var firstOpen = true;
//   var time;

//   $('#timePicker').datetimepicker({
//     useCurrent: false,
//     format: "hh:mm A"
//   }).on('dp.show', function() {
//     if(firstOpen) {
//       time = moment().startOf('day');
//       firstOpen = false;
//     } else {
//       time = "01:00 PM"
//     }

//     $(this).data('DateTimePicker').date(time);
//   });
// })
// });

// function padTo2Digits(num) {
//     return num.toString().padStart(2, "0");
//   }

//   function formatDate(date) {
//     return [
//       padTo2Digits(date.getDate()),
//       padTo2Digits(date.getMonth() + 1),
//       date.getFullYear(),
//     ].join("/");
//   }

//   // 👇️ 24/10/2021 (mm/dd/yyyy)
//   console.log(formatDate(new Date()));

//   //  👇️️ 24/07/2027 (mm/dd/yyyy)
//   console.log(formatDate(new Date(2027, 6, 24)));

//   let realDate = formatDate(new Date());
//   let realDate2 = formatDate(new Date(Date.now() - 7 * 86400000));

//   // $('#dateToShowInGraph').attr('placeholder', realDate2);
//   // $('#dateToShowInGraph2').attr('placeholder', realDate);

// getTravelsInMorning();
// getTravelsInEvening();

// $("#btn1").on("click", function () {
//     var gValue = $(this).attr("class");
//     if (gValue == "btn1") {
//       $("#btn2").removeClass("btn1_all");
//       $("#btn2").addClass("btn1");

//       $("#btn3").removeClass("btn1_all");
//       $("#btn3").addClass("btn1");

//       $(this).removeClass("btn1");
//       $(this).addClass("btn1_all");
//     } else {
//       $(this).removeClass("btn1_all");
//       $(this).addClass("btn1");
//     }
//   });

//   $("#btn2").on("click", function () {
//     var gValue = $(this).attr("class");
//     if (gValue == "btn1") {
//       $("#btn1").removeClass("btn1_all");
//       $("#btn1").addClass("btn1");
//       $("#btn3").removeClass("btn1_all");
//       $("#btn3").addClass("btn1");
//       $(this).removeClass("btn1");
//       $(this).addClass("btn1_all");
//     } else {
//       $(this).removeClass("btn1_all");
//       $(this).addClass("btn1");
//     }
//   });

//   $("#btn3").on("click", function () {
//     var gValue = $(this).attr("class");
//     if (gValue == "btn1") {
//       $("#btn1").removeClass("btn1_all");
//       $("#btn1").addClass("btn1");
//       $("#btn2").removeClass("btn1_all");
//       $("#btn2").addClass("btn1");
//       $(this).removeClass("btn1");
//       $(this).addClass("btn1_all");
//     } else {
//       $(this).removeClass("btn1_all");
//       $(this).addClass("btn1");
//     }
//   });

// function getTravelsInMorning() {
//   $("#btn1").on("click", () => {
//     let startMorning = Date.parse(`${dateSelected} 06:00:00`);
//     let endMorning = Date.parse(`${dateSelected} 12:00:00`);
//     let arrOfMorning = [];
//     arrOfMorning = dataToPutInTable;
//     let arr1 = arrOfMorning.filter((travel) => {
//       return (
//         travel.start_time < endMorning && travel.start_time > startMorning
//       );
//     });
//     let studentsOnchoisenSchoolMorning = [];
//     let counter = 0;

//     // if (choisenSchool !== undefined) {
//     //   arr1.forEach((travel) => {
//     //     if (travel.Passengers) {
//     //       travel.Passengers.forEach((passenger) => {
//     //         if (passenger.school == choisenSchool.name) {
//     //           studentsOnchoisenSchoolMorning.push(passenger);
//     //           if (passenger.confirmed == false) {
//     //             counter += 1;
//     //           }
//     //         }
//     //       });
//     //     }
//     //   });
//     //   let studentNoTConfirmesInThisSchool = counter;
//     //   let studentsNotEntryToThisSchool =
//     //     choisenSchool.student.length -
//     //     studentsOnchoisenSchoolMorning.length;

//     //   donuts(
//     //     studentsOnchoisenSchoolMorning.length,
//     //     studentsNotEntryToThisSchool,
//     //     studentNoTConfirmesInThisSchool
//     //   );

//     //   busOfDonuts(0, 0, 0);
//     // } else {
//     let studentsOnAllBusesOfThisSchool = 0;

//     let studentNoTConfirmesInThisSchool = 0;
//     let counter3 = 0;

//     // document.getElementById("tableTravels").innerHTML = "";
//     arr1.forEach((travel, i) => {
//       if (travel.Passengers) {
//         studentsOnAllBusesOfThisSchool =
//           studentsOnAllBusesOfThisSchool + travel.Passengers.length;
//         travel.Passengers.forEach((passenger) => {
//           if (passenger.confirmed == false) {
//             counter3 += 1;
//           }
//         });
//       }
//     });
//     if ($.fn.DataTable.isDataTable("#example")) {
//       $("#example").DataTable().clear().destroy();
//     }
//     popDataTable($("#example"), arr1, getStudentsList);
//     studentNoTConfirmesInThisSchool = counter3;
//     let studentsNotEntryToThisSchool =
//       AllStudents.data.length - studentsOnAllBusesOfThisSchool;
//     $("#divOfBuses").html(`הסעות ${arr1.length}`);
//     $("#divOfStudents").html(`עלו ${studentsOnAllBusesOfThisSchool}`);
//     donuts(
//       studentsOnAllBusesOfThisSchool,
//       studentsNotEntryToThisSchool,
//       studentNoTConfirmesInThisSchool
//     );
//     let busesOnRoad = arr1.length;

//     busOfDonuts(arr1.length, 0, 0);
//   });
// }
// // console.log(nextminTime);

// function getTravelsInEvening() {
//   $("#btn2").on("click", () => {
//     $("#mymodal4").modal("show");
//     console.log(dataToPutInTable);
//     let startEvening = Date.parse(`${dateSelected} 12:00:00`);
//     let endEvening = Date.parse(`${dateSelected} 18:00:00`);
//     let arrOfEvening = [];
//     console.log(endEvening, endEvening, arrOfEvening);
//     arrOfEvening = dataToPutInTable;
//     let arr2 = arrOfEvening.filter((travel) => {
//       return (
//         travel.start_time > startEvening && travel.start_time < endEvening
//       );
//     });

//     let studentsOnAllBusesOfThisSchool = 0;
//     let studentsNotEntryToThisSchool = 0;
//     let studentNoTConfirmesInThisSchool = 0;
//     let counter = 0;

//     arr2.forEach((travel) => {
//       if (travel.Passengers) {
//         studentsOnAllBusesOfThisSchool =
//           studentsOnAllBusesOfThisSchool + travel.Passengers.length;
//         travel.Passengers.forEach((passenger) => {
//           if (passenger.confirmed == false) {
//             counter += 1;
//           }
//         });
//       }
//     });
//     studentNoTConfirmesInThisSchool = counter;
//     studentsNotEntryToThisSchool =
//       AllStudents.data.length - studentsOnAllBusesOfThisSchool;
//     donuts(
//       studentsOnAllBusesOfThisSchool,
//       studentsNotEntryToThisSchool,
//       studentNoTConfirmesInThisSchool
//     );
//     let busesOnRoad = arr2.length;
//     busOfDonuts(arr2.length, 0, 0);

//     $("#divOfBuses").html(`הסעות ${arr2.length}`);
//     $("#divOfStudents").html(`עלו ${studentsOnAllBusesOfThisSchool}`);

//     console.log(arr2);
//     if ($.fn.DataTable.isDataTable("#example")) {
//       $("#example").DataTable().clear().destroy();
//     }

//     popDataTable("#example", arr2, getStudentsList);

//     $("#mymodal4").modal("hide");
//   });
// }

// $("#btn3").on("click", () => {
//   console.log(dataToPutInTable);
//   $("#divOfBuses").html(`הסעות ${dataToPutInTable.length}`);
//   $("#divOfStudents").html(`עלו ${AllStudentsOnThisDay}`);
//   $("#example").empty();
//   if ($.fn.DataTable.isDataTable("#example")) {
//     $("#example").DataTable().destroy();
//   }
//   popDataTable("#example", dataToPutInTable, getStudentsList);

//   notConfirms = 0;
//   AllStudentsOnThisDay = 0;
//   dataToPutInTable.forEach((travel) => {
//     if (travel.Passengers) {
//       AllStudentsOnThisDay += travel.Passengers.length;
//       travel.Passengers.forEach((passenger) => {
//         if (passenger.confirmed == false) {
//           notConfirms += 1;
//         }
//       });
//     }
//   });

//   donuts(
//     AllStudentsOnThisDay,
//     AllStudentsOfProject - AllStudentsOnThisDay,
//     notConfirms
//   );
//   busOfDonuts(dataToPutInTable.length, 0, 0);
// });

// const selectStartDate = document.querySelector("#startDate");

// selectStartDate.addEventListener("change", (event) => {
//   dateStart = event.target.value;
//    newdateStart = Date.parse(dateStart.substring(0,10) + ' ' +startDate.substring(12));
//   console.log(newdateStart);

// });

// const selectEndDate = document.querySelector("#endDate");

// selectEndDate.addEventListener("change", (event) => {
//   dateSelectedEnd = event.target.value;
//    newdateEnd = Date.parse(dateStart.substring(0,10) + ' ' +startDate.substring(12));
//   console.log(newdateStart);
// });

// console.log(newdateStart);
// console.log(newdateEnd);

// const selectStartDate = document.querySelector("#startDate");

// selectStartDate.addEventListener("change", (event) => {
//   startDate = event.target.value;
//   console.log(startDate);
//   let ddd = startDate.substring(0,10) + ' ' +startDate.substring(12)
//   console.log(ddd);

//   let newminTime = Date.parse(ddd);
//   console.log(newminTime)
// });

// const selectEndDate = document.querySelector("#endDate");

// selectEndDate.addEventListener("change", (event) => {
//   dateSelectedEnd = event.target.value;
//   console.log(dateSelectedEnd);
//   let ddd = startDate.substring(0,10) + ' ' +startDate.substring(12)
//   // let newminTime = Date.parse(`${startDate} 06:00:00`);
//   let newmaxTime = Date.parse(dateSelectedEnd);
//   console.log(newmaxTime)
// });

/* summary of every school  */

// let container = document.getElementById("schools");
// container.innerHTML = "";
// function compare(a, b) {
//   return b.students_on_bus - a.students_on_bus;
// }

// let arrayOfSchools = Object.values(schools);
// let newArraySchools = arrayOfSchools.sort(compare);
// // console.log(newArraySchools);
// newArraySchools.forEach(function (school, i) {
//   let div_school = document.createElement("li");
//   let name = document.createElement("div");
//   let summary_div = document.createElement("div");
//   let numberOfSummary = document.createElement("div");
//   let Buses_div = document.createElement("div");
//   let studentsOnBus = document.createElement("div");
//   let travelsWithExeption = document.createElement("div");
//   let AllTravelsOfSchool = document.createElement("div");

//   div_school.classList.add("div_school");
//   div_school.setAttribute("data-school-name", school.name);
//   name.classList.add("name_school");
//   // name.setAttribute("data-school-name", school.name);
//   // summary_div.setAttribute("data-school-name", school.name);
//   // Buses_div.setAttribute("data-school-name", school.name);

//   // name.setAttribute("id", `schoolNumber_${i}`);
//   name.appendChild(document.createTextNode(school.name));

//   // let div = document.getElementById(school.name);

//   div_school.appendChild(name);

//   // let busesOfSchool = school.travel.length + " הסעות  ";
//   let busesOfSchool = 0 + " הסעות  ";

//   Buses_div.appendChild(document.createTextNode(busesOfSchool));

//   AllTravelsOfSchool.appendChild(Buses_div);
//   AllTravelsOfSchool.classList.add("AllTravelsOfSchool");

//   numberOfSummary.classList.add("numOfSummary_school");

//   let summary = school.student.length + " תלמידים ";
//   let numberOfStudentsOnBus = school.students_on_bus + " עלו  ";
//   studentsOnBus.appendChild(
//     document.createTextNode(numberOfStudentsOnBus)
//   );
//   studentsOnBus.classList.add("studentsOnBus");
//   numberOfSummary.appendChild(document.createTextNode(summary));

//   numberOfSummary.classList.add("numberOfSummary");
//   numberOfSummary.setAttribute("id", school.name);

//   let busesOnRoad = 0;
//   let BusesFinish = 0;
//   let BusesWithExeption = 0;

//   if (BusesWithExeption > 0) {
//     travelsWithExeption.appendChild(
//       document.createTextNode(BusesWithExeption + " בחריגה ")
//     );
//     travelsWithExeption.classList.add("travelsWithExeption");
//     AllTravelsOfSchool.appendChild(travelsWithExeption);
//   }
//   summary_div.appendChild(numberOfSummary);
//   summary_div.appendChild(studentsOnBus);

//   summary_div.classList.add("summary_div");
//   // summary_div.appendChild(btn);

//   div_school.setAttribute("id", `schoolNumber_${i}`);
//   div_school.appendChild(AllTravelsOfSchool);
//   div_school.appendChild(summary_div);
//   container.appendChild(div_school);
//   div_school.addEventListener("click", choiseSchool);
// });

// /* details for the chart */
// let studentsOnBus = Object.values(schools);
// let counter2 = 0;
// ArrOfAllTravels = [];

// studentsOnBus = studentsOnBus.forEach((school) => {
//   if (school.students_on_bus.length > 0) {
//     counter2 += school.students_on_bus;
//   }
// });
// studentsOnBus = counter2;
// let notConfirms = 0;
// let busesOnRoad = Alltravels.length;
// let BusesFinish = 0;
// let BusesWithExeption = 0;

// let AlltravelsFromSchool = [];
// let Allschools = Object.values(schools);
// Allschools.forEach((school) => {
//   if (school.travel.length > 0) {
//     ArrOfAllTravels = AlltravelsFromSchool.concat(school.travel);
//   }
// });

// let arrayOfTravelsfromEfrat = [];
// travelsFromEfrat.forEach((travel) => {
//   // travel.vid = travel.vid.replace("מועצה מקומית אפרת/", "");
//   console.log(travel);
//   let travelfromefrat = new Travel_(travel);
//   travelfromefrat.Passengers = [];
//   arrayOfTravelsfromEfrat.push(travelfromefrat);
// });

// let arrayEfratWithoutDuplicated = arrayOfTravelsfromEfrat.filter(
//   function (travel) {
//     return (
//       arrayOfUndefined.find(function (travel2) {
//         return travel.vid === travel2.vid;
//       }) === undefined
//     );
//   }
// );

// AllTravelsWithUndefined = ArrOfAllTravels.concat(arrayOfUndefined);
// AllOfTracks = AllTravelsWithUndefined.concat(
//   arrayEfratWithoutDuplicated
// );
// console.log(AllOfTracks);

// dataToPutInTable = AllOfTracks;

// notConfirms = 0;

// dataToPutInTable.forEach((travel) => {
//   if (travel.Passengers) {
//     AllStudentsOnThisDay += travel.Passengers.length;
//     travel.Passengers.forEach((passenger) => {
//       if (passenger.confirmed == false) {
//         notConfirms += 1;
//       }
//     });
//   }
// });

// getTravelsInMorning();
// getTravelsInEvening();


 // dateSelected = $.datepicker.formatDate("dd-mm-yy", new Date());

      // var myDate = new Date();
      // dateSelected = myDate.getFullYear() + '-' + ('0'+ myDate.getMonth()+1).slice(-2) + '-' + ('0'+ myDate.getDate()).slice(-2);
      // console.log(dateSelected)
      // $("#datepicker").val(dateSelected);
      // $("#datepicker2").val(dateSelected);

      // console.log(dateSelected);
      // dateOfToday = dateSelected;

      // $("#datepicker2")
      //   .datepicker({
      //     // dateFormat: "mm-dd-yy",
      //     onSelect: function (dateText, inst) {
      //       // $("input[name='datePeacker2']").val(dateText);
      //       // if (dateText == dateOfToday) {
      //         // dateSelected = dateText;
      //         dateStart = new Date(dateText).getTime();
      //         console.log(dateStart);
      //         // updateData(nextminTime);
      //       // } else {
      //       //   dateSelected = dateText;
      //       //   console.log(dateSelected);
      //       //   historyTravels();
      //       // }
      //     },
      //   })
      // .datepicker("setDate", dateSelected );

      //   let dateSelectedEnd;
      // $("#datepicker").datepicker({
      //   // dateFormat: "mm-dd-yy",
      //   onSelect: function (dateText, inst) {
      //     $("input[name='datePeacker']").val(dateText);
      //     // if (dateText == dateOfToday) {
      //       dateSelectedEnd = dateText;

      //       updateData(nextminTime);
      //     // } else {
      //       dateSelectedEnd =  new Date(dateText).getTime();
      //       console.log(dateSelectedEnd);
      //       historyTravels();
      //     // }
      //   },
      // })
      // .datepicker("setDate", dateSelected);

      // const input = document.querySelector('startDate');
      // const input2 = document.querySelector('endDate');
      // input.addEventListener('change', updateValue);
      // input2.addEventListener('change', updateValue);

      // function updateValue(e) {
      //   console.log (e.target.value);
      // }

      // $("#startDate").on("change", () => {
      //   let dateStart2 = $("#startDate").value;
      //   let dateEnd = $("#endDate").value;
      //   console.log(dateStart2);
      //   console.log(dateEnd);
      // });

      // $("#changeDates").on("click", function changeDates() {
      //   let dateStart2 = $("#startDate").value;
      //   let dateEnd = $("#endDate").value;
      //   console.log(dateStart2);
      //   console.log(dateEnd);
      // });

          // Allschools.forEach((school) => {
        //   if (school.travel.length > 0) {
        //     ArrOfAllTravels = AlltravelsFromSchool.concat(school.travel);
        //   }
        // });

        // let arrayOfTravelsfromEfrat = [];
        // travelsFromEfrat.forEach((travel) => {
        //   // travel.vid = travel.vid.replace("מועצה מקומית אפרת/", "");
        //   console.log(travel);
        //   let travelfromefrat = new Travel_(travel);
        //   travelfromefrat.Passengers = [];
        //   arrayOfTravelsfromEfrat.push(travelfromefrat);
        // });

        // let arrayEfratWithoutDuplicated = arrayOfTravelsfromEfrat.filter(
        //   function (travel) {
        //     return (
        //       arrayOfUndefined.find(function (travel2) {
        //         return travel.vid === travel2.vid;
        //       }) === undefined
        //     );
        //   }
        // );

        // AllTravelsWithUndefined = ArrOfAllTravels.concat(arrayOfUndefined);
        // AllOfTracks = AllTravelsWithUndefined.concat(
        //   arrayEfratWithoutDuplicated
        // );
        // console.log(AllOfTracks);
        // console.log(Alltravels);



           // if (data.data["new pickup rides"].length > 0) {
    //   isChange = true;
    //   data.data["new pickup rides"].forEach((travel) => {
    //     let updateTravel = new Travel_(travel);
    //     newPickupTravels.push(updateTravel);
    //   });
    // }
    // // let newUndefinedTravels = undefinedTravels.concat(
    // //   data.data["new undefined rides"]
    // // );
    // newAllTravelsWithNewPickup =
    //   AllTravelsWithUndefined.concat(newUndefinedTravels);

    // if (data.data["new pickups"].length > 0) {
    //   isChange = true;
    //   data.data["new pickups"].forEach((passenger) => {
    //     Object.values(schools).forEach((school) => {
    //       let passengerExist = school.travel.find(
    //         (travel) => travel.job_id == passenger.job_id
    //       );
    //       passengerExist.Passengers.push(passenger);
    //       school.students_on_bus += 1;
    //     });
    //   });
    // }

    // data.data["new undefined rides"].forEach((travel) => {
    //   let existTravel = newAllTravelsWithUndefined.find(
    //     (item) => item.vid == travel.vid
    //   );

    //   if (existTravel) {
    //     isChange = true;
    //   }
    //   if (existTravel.Passengers.length < travel.Passengers.length) {
    //     isChange = true;
    //     let index = newAllTravelsWithUndefined.findIndex(
    //       (item) => item.vid == travel.vid
    //     );
    //     newAllTravelsWithUndefined.splice(index - 1, 1);
    //     newAllTravelsWithUndefined.push(travel);
    //   }
    // });

    // if (
    //   isChange == true
    //   // data.data["new rides"].length > 0 ||
    //   // data.data["new ends"].length > 0 ||
    //   // data.data["new undefined rides"].length > 0 ||
    //   // data.data["new pickups"].length > 0
    // ) {
    //   AllTravelsWithUndefined = newAllTravelsWithUndefined;
    //   initData(nextminTime);
    // } else {
    //   updateData();
    // }
    //   },
    // ;





    
      // createTableRow(i) {
      //   let rides = this.rides ? this.rides.length : "1";
      //   let StudentsOnBus;
      //   let confirms = 0;
      //   let tr = document.createElement("tr");
      //   // if (this.has_spm == false){}
      //   if (this.Passengers) {
      //     StudentsOnBus = this.Passengers.length ? this.Passengers.length : 0;
      //     confirms = this.Passengers.filter(
      //       (passenger) => passenger.confirmed == false
      //     );
      //   }
      //   let count = 0;

      //   let entryTime = new Date(this.start_time);

      //   let time =
      //     entryTime
      //       .getHours()
      //       .toString()
      //       .replace(/^(\d)$/, "0$1") +
      //     ":" +
      //     entryTime
      //       .getMinutes()
      //       .toString()
      //       .replace(/^(\d)$/, "0$1");
      //   if (entryTime == "Invalid Date") {
      //     time = "-";
      //   }

      //   let TimeInDestination = new Date(this.predicted_arrival_time);
      //   let newTimeInDestination =
      //     TimeInDestination.getHours()
      //       .toString()
      //       .replace(/^(\d)$/, "0$1") +
      //     ":" +
      //     TimeInDestination.getMinutes()
      //       .toString()
      //       .replace(/^(\d)$/, "0$1");
      //   if (entryTime == "Invalid Date") {
      //     newTimeInDestination = "-";
      //   }

      //   [
      //     this.vid || "-",
      //     this.driver_name || "-",
      //     time || "-",

      //     newTimeInDestination,

      //     StudentsOnBus || "-",
      //     confirms.length || "-",
      //   ].forEach((b, i) => {
      //     let td = document.createElement("td");
      //     td.setAttribute("id", `td${i}`);
      //     td.appendChild(document.createTextNode(b));
      //     tr.appendChild(td);
      //   });

      //   let studentsImage = document.createElement("img");
      //   studentsImage.setAttribute(
      //     "src",
      //     "../lib/Images/students_images/iconStudents.svg"
      //   );
      //   studentsImage.setAttribute("id", `studentsList_${i}`);
      //   let tdImg = document.createElement("td");
      //   if (!StudentsOnBus > 0) {
      //     tdImg.classList.add("grey");
      //   }
      //   tdImg.appendChild(studentsImage);
      //   tr.appendChild(tdImg);

      //   let tdMap = document.createElement("td");
      //   let mapIcon = document.createElement("img");
      //   mapIcon.setAttribute(
      //     "src",
      //     "../lib/Images/students_images/iconMap.svg"
      //   );
      //   tdMap.appendChild(mapIcon);
      //   tr.appendChild(tdMap);

      //   $(document).on("click", `#studentsList_${i}`, () => {
      //     getStudentsList(this);
      //   });
      //   if (this.comment == "without SPM") {
      //     tr.classList.add("no_spm");
      //   }
      //   return tr;
      // }



      // handleClick(school) {
      //   // console.log(school);
      //   dataToPutInTable = school.travel;
      //   let tbody = document.getElementById("tableTravels");

      //   tbody.innerHTML = "";
      //   school.travel.forEach(function (travel, i) {
      //     tbody.appendChild(travel.createTableRow(i));
      //   });
      //   // if ($.fn.DataTable.isDataTable("#example")) {
      //   //   $("#example").DataTable().destroy();
      //   // }
      //   popDataTable($("#example"));
      // }


      // let notConfirms = 0;
      // let BusesFinish = 0;
      // let BusesWithExeption = 0;
      // let busesOnRoad = 0;

      // school.travel.forEach((travel) => {
      //   switch (travel.finished) {
      //     case true:
      //       BusesFinish += 1;
      //       break;
      //     case false:
      //       if (travel.predicted_arrival_time < Date.now()) {
      //         BusesWithExeption += 1;
      //       } else {
      //         busesOnRoad += 1;
      //       }
      //       break;
      //   }
      //   if (travel.Passengers) {
      //     travel.Passengers.forEach((passenger) => {
      //       if (passenger.confirmed == false) {
      //         notConfirms += 1;
      //       }
      //     });
      //   }
      // });

      // let NoEntryToBus = school.student.length - school.students_on_bus || 0;
      // console.log(NoEntryToBus);
      // console.log(notConfirms);
      // donuts(school.students_on_bus, NoEntryToBus, notConfirms);
      // busOfDonuts(0, 0, 0);


            // let busesOnRoad = Alltravels.length;
      // let BusesFinish = 0;
      // let BusesWithExeption = 0;

      // let AlltravelsFromSchool = [];
      // let Allschools = Object.values(schools);

      // let newAllTravels = [];

      // Alltravels.forEach((travel) => {
      //   let updateTravel = new Travel_(travel);

      //   if (travel.Passengers) {
      //     if (travel.Passengers.length < 10) {
      //       travel.notRegular = true;
      //       BusesNoRegular += 1;
      //     } else {
      //       travel.notRegular = false;
      //       BusesRegular += 1;
      //     }
      //   } else {
      //     BusesNoStudents += 1;
      //   }

      //   newAllTravels.push(updateTravel);
      // });
      // console.log(newAllTravels);

       // function choiseTrack() {
  //   let number = this.getAttribute("number");
  //   console.log(AllTravelsWithUndefined);
  //   let TrackInMap = AllTravelsWithUndefined.find(
  //     (travel) => travel.vid == number
  //   );
  //   console.log(TrackInMap);
  //   let ul = document.getElementById("tracks");
  //   let li = ul.getElementsByTagName("li");
  //   for (let i = 0; i < li.length; i++) {
  //     li[i].style.background = "";
  //   }
  //   this.style.background = "#CBE0E9";

  //   let map;
  //   $("#box3").empty();
  //   map = new google.maps.Map(document.getElementById("box3"), {
  //     center: { lat: this.lat, lng: this.lng },
  //     zoom: 15,
  //   });
  //   const image = "../../../web/prod/lib/Images/students_images/bus.png";
  //   // for (let i = 0; i < trackList.length; i++) {
  //   new google.maps.Marker({
  //     position: { lat: this.lat, lng: this.lng },
  //     map,
  //     icon: image,
  //     title: "Hello World!",
  //   });

  //   // }
  // }

  // $("#showTravels").on("click", showTravels);

  // function showTravels() {
  //   $("#box3").empty();

  //   // dataToPutInTable = AllOfTracks;
  //   $("#tableTravels").empty();
  //   dataToPutInTable.forEach((travel, i) => {
  //     let tbody = document.getElementById("tableTravels");
  //     tbody.appendChild(travel.createTableRow(i));
  //   });
  //   if ($.fn.DataTable.isDataTable("#example")) {
  //     $("#example").DataTable().destroy();
  //   }
  //   popDataTable("#example");

  //   getBus(AllOfTracks);
  //   // getBus(datatest);
  //   donuts(
  //     StudentsOnAllBuses,
  //     AllStudentsOfProject - StudentsOnAllBuses,
  //     notConfirms
  //   );
  //   busOfDonuts(
  //     AllOfTracks.length || 0,
  //     BusesFinish || 0,
  //     BusesWithExeption || 0
  //   );
  // }


  // function getStudentsList(travel) {
  //   $("#myModal").modal("show");
  //   $("#myModal").modal({
  //     backdrop: "static",
  //     keyboard: false,
  //   });
  //   $("#modalTable").empty();
  //   let countOfNoConfirms = [];
  //   if (travel.Passengers) {
  //     countOfNoConfirms = travel.Passengers.filter(
  //       (passenger) => passenger.confirmed == false
  //     );

  //     $("#modalNoConfirms").html(` דחויים  ${countOfNoConfirms.length} `);
  //   }
  //   let sum = travel.Passengers ? travel.Passengers.length : 0;
  //   $("#totalModal").html(`סה"כ עלו ${sum} תלמידים`);

  //   $("#numberOfTrack").html(`${travel.vid}`);

  //   let today = new Date();
  //   let realDate = today.toLocaleDateString("en-US");
  //   $("#realDate").html(realDate);
  //   let updateAllStudents = [];
  //   travel.Passengers.forEach((student) => {
  //     if (updateAllStudents.length > 0) {
  //       updateAllStudents.forEach((passenger) => {
        
  //         let tr = document.createElement("tr");
  //         let pickupTime = new Date(passenger.pickup_time);

  //         let time =
  //           pickupTime
  //             .getHours()
  //             .toString()
  //             .replace(/^(\d)$/, "0$1") +
  //           ":" +
  //           pickupTime
  //             .getMinutes()
  //             .toString()
  //             .replace(/^(\d)$/, "0$1");

  //         if (time == "undefined") {
  //           time = "-";
  //         }

  //         let sign =
  //           passenger.confirmed == true
  //             ? `<span id="signTrue">V</span>`
  //             : `<span id="signFalse">x</span>`;

  //         [
  //           passenger["Full name"], passenger.CardNumber,
  //           passenger.school,
  //           passenger.grade,
  //           time,
  //         ].forEach((detail) => {
  //           let td = document.createElement("td");
  //           td.append(document.createTextNode(detail));
  //           tr.appendChild(td);
  //         });
  //         let td = document.createElement("td");
  //         td.innerHTML = sign;
  //         tr.appendChild(td);
  //         $("#modalTable").append(tr);
  //       });
  //     }
  //   });
  // }


  // function getBus(AllOfTracks) {
  //   $("#inputBuses").on("keyup", function searchInputStudent(myUl, myLi, myA) {
   
  //     let input, filter, ul, li, a, i, txtValue;
  //     input = document.getElementById("inputBuses");
  //     filter = input.value;
  //     ul = document.getElementById("schools");
  //     li = ul.getElementsByTagName("li");
  
  //     for (i = 0; i < li.length; i++) {
  //       a = li[i];
  //       txtValue = a.getAttribute("data-school-name");
  //       if (txtValue.indexOf(filter) > -1) {
  //         li[i].style.display = "";
  //       } else {
  //         li[i].style.display = "none";
  //       }
  //     }
  //   });
  
  //   $("#searchStudent").keyup(function () {
  //     datatable.search($(this).val()).draw();
  //   });
  
  
  // }